import { PlotObs } from './plot.js';
import { getTrans } from './strings.js';
import { modal } from './utils.js';

/*
 * Number widget to reflect the number of unsynchronised PlotObs.
 */
class UnsyncCounter {
    constructor(db) {
        this.count = 0;
        this.db = db;
        this.div = document.getElementById('unsync-counter');
        var self = this;
        db.get("new-plotobs").then(doc => {
            self.setCount(doc.newIDs.length);
        }).catch(err => {
            self.setCount(0);
        });
        // Allow force-resync of all local ksp data
        this.div.addEventListener('dblclick', async (ev) => {
            var resp = await modal.confirm(getTrans('resyncconfirm'));
            if (resp != 'ok') return;
            var promises = [];
            const thisYear = new Date().getFullYear();
            var result = await db.allDocs({include_docs: true, startkey: 'plotobs'});
            for (let i=0; i<result.rows.length; i++) {
                if (result.rows[i].id.indexOf('-' + thisYear) >=0) {
                    var po = new PlotObs(null, result.rows[i].doc);
                    if (po.isFinished()) promises.push(po.sendToServer());
                }
            }
            await Promise.all(promises);
            modal.alert(promises.length + getTrans('syncpoints'));
        });
    }

    static _counter = null;

    setCount(count) {
        if (count < 0) count = 0;
        this.count = count;
        this.div.textContent = this.count;
        if (this.count > 0) {
            this.div.classList.add("nonempty");
        } else {
            this.div.classList.remove("nonempty");
        }
    }

    increment() { this.setCount(this.count + 1); }
    decrement() { this.setCount(this.count - 1); }
}

export function initCounter(db) {
    UnsyncCounter._counter = new UnsyncCounter(db);
    return UnsyncCounter._counter;
}

export function getCounter() {
    return UnsyncCounter._counter;
}
