import { activeAngleUnit, beep_error, beep_success, computeCoordDifference,
         epsg2056Converter, getPlotObsFormSteps, insertTableRow, modal, rounded,
         setFormDefaultFromProperties, toggleImg } from './utils.js';
import { TreeForm } from './treeobs.js';
import { checkLonLatDifference } from './map.js';
import { getTrans } from './strings.js';

const thisYear = new Date().getFullYear(),
      NEW_TREE_STEP = 'tree-new';


/* Set of input forms */
export class FormSet {
    treeFields = {
        species: 'species',
        diameter: 'dbh',
        distance: 'distance',
        azimuth: 'azimuth',
        marked: 'marked'
    }

    constructor(app, TreeFormKlass) {
        this.app = app;
        this.forms = document.querySelectorAll('form.input-form');
        if (typeof TreeFormKlass !== 'undefined') this.treeForm = new TreeFormKlass(this);
        else this.treeForm = new TreeForm(this);
        this.FINAL_STEP = 'final';
        this.speciesSelectId = 'id_species';
        this.fixedLine = null;
    }

    /* Called once from DOMContentLoaded event.*/
    initHandlers() {
        // Form submit buttons
        for (var i = 0; i < this.forms.length; i++) {
            this.forms[i].addEventListener('submit', this.submitForm.bind(this));
        }
        // When clicking the new tree or weiter buttons, set hint on the form about
        // which button was clicked.
        var ntButtons = document.querySelectorAll('button.newtree');
        for (var i = 0; i < ntButtons.length; i++) {
            ntButtons[i].addEventListener('click', (ev) => {
                ev.target.closest('form').dataset.nextform = "newtree";
            });
        }
        var wButtons = document.querySelectorAll('button.weiter');
        for (var i = 0; i < wButtons.length; i++) {
            wButtons[i].addEventListener('click', (ev) => {
                ev.target.closest('form').dataset.nextform = "nextstep";
            });
        }
        // Button to keep current GPS coordinates in form inputs
        document.getElementById('keepthis').addEventListener('click', (ev) => {
            ev.preventDefault();
            document.getElementById('id_exact_long').value = document.getElementById('currentLong').innerHTML;
            document.getElementById('id_exact_lat').value = document.getElementById('currentLat').innerHTML;
            const accDiv = document.getElementById('accuracyValue');
            if (accDiv && document.getElementById('id_gps_precision')) {
                const precValue = accDiv.textContent.replace('m', '');
                document.getElementById('id_gps_precision').value = precValue;
            }
            checkLonLatDifference(this.app.currentPlot);
        });
        document.getElementById('btn-fixpunkt').addEventListener('click', (ev) => {
            ev.preventDefault();
            this.setUnsetFixpoint();
        });
        document.getElementById('id_exact_long').addEventListener('input', (ev) => { checkLonLatDifference(this.app.currentPlot); });
        document.getElementById('id_exact_lat').addEventListener('input', (ev) => { checkLonLatDifference(this.app.currentPlot); });

        const edgef_h = document.getElementById('forest_edgef_h');
        if (edgef_h) {
            edgef_h.addEventListener('input', (ev) => {
                let edgef = computeEdgeFactor(this.app.currentPlot.properties.radius, ev.target.value);
                document.getElementById('id_forest_edgef').value = edgef;
            });
        }

        this.treeForm.initHandlers(this.app);
    }

    submitForm(ev, form) {
        var form = form || ev.target;
        if (ev) ev.preventDefault();
        if (this.validateForm(form)) {
            if (form.dataset.nextform == "newtree") {
                this.showTreeStep(null);
            } else {
                this.goToNextStep();
            }
        }
    }

    hideAll() {
        for (var i = 0; i < this.forms.length; i++) {
            this.forms[i].style.display = 'none';
        }
        document.getElementById('allsteps').style.display = 'none';
        if (this.app.caliper) {
            document.getElementById('caliper-disconnected').setAttribute('hidden', true);
            document.getElementById('caliper-connected').setAttribute('hidden', true);
        }
        if (this.fixedLine) {
            this.setUnsetFixpoint();
        }
        this.app.map.unHighlight();
    }

    startInput(obs) {
        // Show start screen for a new observation
        document.getElementById('obs-data').style.display = 'none';
        document.getElementById('obs-input').style.display = 'block';
        if (obs) {
            // We have a partially-filled PlotObs
            document.getElementById('step0').style.display = 'none';
            this.initForms(obs);
        } else {
            document.getElementById('step0').style.display = 'block';
        }
    }

    /*
     * Initialize plot obs input forms and show first or next unfilled form.
     */
    initForms(obs) {
        const currentPlot = this.app.currentPlot;
        // Reset all input forms
        for (var i=0; i < this.forms.length; i++) {
            this.resetForm(this.forms[i]);
        }
        const is_new = (obs === null);
        if (is_new) {
            obs = currentPlot.createObs(thisYear);
        }
        obs.showTrees(this.app.map, true);
        obs.setUpStepSelect();
        // Set inventory team
        this.app.db.get('current-state').then((data) => {
            const inventoryInput = document.querySelector('#id_protocol, #id_inv_team');
            if (inventoryInput && data.inventory) inventoryInput.value = data.inventory;
        });
        // Set initial values for non-tree form widgets
        var previousObs = currentPlot.previousObs(thisYear);
        var inheritedProps = {};
        if (is_new) {
            // The first form has mixed plot and plotobs properties
            // currentPlot wins for some props like Entwicklungstufe, Mischungsgrad and Schlussgrad
            if (previousObs) {
                inheritedProps = Object.assign(
                    {}, previousObs.properties(), currentPlot.properties
                );
            } else {
                inheritedProps = Object.assign({}, currentPlot.properties);
            }
            delete inheritedProps.remarks;  // TODO: check with VD
        }
        for (var step = 1; step <= getPlotObsFormSteps(); step++) {
            var form = document.getElementById('step' + step);
            this.initForm(form, obs, is_new, is_new ? inheritedProps : obs.properties());
        }

        // Insert previous remarks above the remarks form input
        var form4Body = document.getElementById('previous-remarks');
        form4Body.innerHTML = '';
        while (previousObs !== null) {
            var remarks = previousObs.properties().remarks;
            if (remarks !== undefined && remarks.length > 0) {
                var row = form4Body.insertRow(0);
                var th = document.createElement('th');
                th.appendChild(document.createTextNode(previousObs.year + ':'));
                row.appendChild(th);
                var cell2 = row.insertCell(1);
                cell2.appendChild(document.createTextNode(remarks));
            }
            previousObs = currentPlot.previousObs(previousObs.year);
        }

        this.showStep(obs.fillStep);
    }

    initForm(form, obs, isNew, properties) {
        setFormDefaultFromProperties(form, properties);
    }

    resetForm(form) {
        for (var i = 0; i < form.elements.length; i++) {
            let element = form.elements[i];
            if (['submit', 'button'].indexOf(element.type) >= 0 || element.type == 'hidden') continue;
            if (element.type == 'radio'){
                element.checked = element.value == '';
            } else if (element.type == 'checkbox') {
                element.checked = false;
            } else if (element.type == 'select-one') {
                element.value = "";
                // Find any default value
                for (var j = 0; j < element.options.length; j++) {
                    if (j > 0 && element.options[j].defaultSelected) {
                        element.value = element.options[j].value;
                        break;
                    }
                }
            }
            else {
                element.value = "";
            }
        }
    }

    validateForm(form) {
        if (form === null) form = this.getActive();
        if (form.id === "stepfinal") return true; // Nothing to validate here

        var obs = this.app.currentObs();
        let currentStep = form.dataset.step;
        // Get form target, PlotObs or TreeObs
        if (form.treeObject) {
            target = form.treeObject;
        } else if (currentStep.startsWith('tree'))  {
            target = obs.addTreeObs(null);
        } else {
            var target = obs;
        }
        // Set form values to either PlotObs or TreeObs
        for (var i = 0; i < form.elements.length; i++) {
            let element = form.elements[i];
            if (!element.name.length) continue;
            var value = element.value;
            if (element.name == 'azimuth') {
                value = TreeForm.convertAzimuth(value);
            }
            if (element.tagName == "SELECT") {
                if (value != '') {
                    value = [value, element[element.selectedIndex].textContent];
                } else {
                    value = [value, value];
                }
            } else if (element.type == 'checkbox') {
                value = element.checked;
            } else if (element.type == 'radio') {
                if (!element.checked) continue;
                value = [element.value, element.nextElementSibling.textContent];
            }
            if (element.name == 'remarks' && target == obs) {
                // Remarks may be twice, sync the contents
                document.querySelectorAll('#id_remarks').forEach(inp => inp.value = value);
            }
            target.setProperty(element.name, value);
        }
        // Checking validity after saving values, so input are not lost even if validation fails
        var valid = form.reportValidity();
        if (!valid) return false;

        // Mark tree as validated
        if (target !== obs) {
            target.validated = true;
            target.showOnMap(this.app.map, 'treeLayer', true);
        }
        obs.stepFilled(currentStep);
        obs.saveLocally();
        this.app.map.unHighlight();
        return true;
    }

    goToNextStep() {
        // The next step is the next allstepsselect item.
        const stepSelect = document.getElementById('allstepsselect');
        const nextOption = stepSelect.options[stepSelect.selectedIndex + 1];
        if(nextOption) this.showStep(nextOption.value);
        else this.showStep('stepfinal');
    }

    allStepEnabled() {
        const stepSelect = document.getElementById('allstepsselect');
        return Array.from(stepSelect.options).every(opt => !opt.hasAttribute("disabled"));
    }

    showStep(step) {
        var obs = this.app.currentObs();
        // Select and display the form matching step.
        if (step == this.FINAL_STEP) {
            var form = document.getElementById('stepfinal');
        } else if (step <= getPlotObsFormSteps()) {
            var form = document.getElementById('step' + step);
        } else {
            step = this.FINAL_STEP;
            var form = document.getElementById('stepfinal');
        }
        const stepSelect = document.getElementById('allstepsselect');
        stepSelect.value = step.startsWith('tree') ? this.FINAL_STEP.toString() : step.toString();

        if (form.style.display == 'block') return;  // form already active
        this.hideAll();
        document.getElementById('allsteps').style.display = 'block';
        form.style.display = 'block';
        // Un-disable any select option before or matching this step
        for (var i=0; i < stepSelect.options.length; i++) {
            if (parseInt(stepSelect.options[i].value) <= step.toString()) {
                stepSelect.options[i].removeAttribute("disabled");
            }
        }
        if (step == this.FINAL_STEP) {
            stepSelect.options[stepSelect.options.length - 1].removeAttribute("disabled");
            this.showTreeListStep(obs);
            if (this.app.caliper) this.app.caliper.showStatus();
        }
        // Must come after values have been populated, as locks depend on values.
        this.resetLockInputs();
    }

    showTreeStep(tree) {
        const form = document.getElementById('steptree');
        var obs = tree ? tree.plotobs : this.app.currentObs();
        this.hideAll();
        form.style.display = 'block';

        if (tree == null) {
            const hasPreviousObs = obs.plot.previousObs(thisYear) !== null;
            this.showTree(null, !hasPreviousObs);
        } else {
            // An existing tree (copied from previous obs or recently added).
            this.showTree(tree, false);
        }
        form.treeObject = tree;
        //this.treeForm.dataset.step = step;
        if (this.app.caliper) this.app.caliper.showStatus();
        this.resetLockInputs();
    }

    showTreeListStep(obs) {
        const form = document.getElementById('stepfinal');
        if (obs.allTreesValidated()) {
            form.classList.add('ready');
            form.querySelector('#input-end').removeAttribute('disabled');
        } else {
            form.classList.remove('ready');
            form.querySelector('#input-end').setAttribute('disabled', true);
        }
        // Populate a summary of the trees
        var treeTable = document.getElementById('trees_summary');
        var oldTbody = treeTable.children[1];
        var newTbody = document.createElement('tbody');
        obs.trees.forEach((tree, index) => {
            this.showTreeInStepTreeList(tree, index, newTbody);
        });
        document.getElementById('trees_total').textContent = `(${obs.trees.length})`;
        oldTbody.parentNode.replaceChild(newTbody, oldTbody);
        newTbody.addEventListener('click', (ev) => {
            const treeTR = ev.target.closest('tr');
            if (treeTR) {
                // go to matching tree step
                this.showTreeStep(treeTR.treeObject);
            }
        });

        Array.from(document.getElementsByClassName('treenumb')).forEach(
            numb => { numb.classList.remove('hidden'); }
        );
    }

    /*
     * If target inputs have no value set, hide locks, set input disabled to false.
     * If target inputs have values, display lock, reset to lock image, set input
     * disabled to true.
     */
    resetLockInputs() {
        document.querySelectorAll('img.locked-input').forEach(lock => {
            var input = lock.previousElementSibling;
            if (input && input.tagName == 'SPAN') input = input.previousElementSibling;
            if (input && input.value != '') {
                if (lock.src.indexOf('unlocked') >= 0) toggleImg(lock);
                lock.style.display = 'inline';
                input.disabled = true;
            } else {
                lock.style.display = 'none';
                if (input) input.disabled = false;
            }
        });
    }

    setUnsetFixpoint() {
        const btn = document.getElementById('btn-fixpunkt');
        if (btn.textContent.trim() == btn.dataset.title1) {
            const plot = this.app.currentPlot;
            // Draw line from exact coords to theoric coords
            const start = [
                document.getElementById('currentLong').textContent,
                document.getElementById('currentLat').textContent
            ];
            //DEBUG:const start = [plot.properties.coords_2056[0] + 15, plot.properties.coords_2056[1] - 12],
            const startConv = epsg2056Converter.inverse(start),
                  center = plot.properties.coords_2056,
                  centerConv = epsg2056Converter.inverse(center);
            const latlngs = [
                [startConv[1], startConv[0]],
                [centerConv[1], centerConv[0]]
            ];
            this.fixedLine = L.polyline(latlngs, {color: 'yellow'}).addTo(this.app.map.map);
            const diff = computeCoordDifference(start, center);
            this.fixedLine.bindTooltip(
                `${diff.distance}m, ${diff.angle.value}${diff.angle.symbol}`,
                {permanent: true, opacity: 0.6}
            );
            document.getElementById('fixpunkt-prec').textContent = document.getElementById('accuracyValue').textContent;
            document.getElementById('fixpunkt-dist').textContent = `${diff.distance}m`;
            document.getElementById('fixpunkt-azim').textContent = `${diff.angle.value}${diff.angle.symbol}`;
            document.getElementById('fixpunkt-data').removeAttribute('hidden');
            btn.textContent = btn.dataset.title2;
        } else {
            document.getElementById('fixpunkt-data').setAttribute('hidden', true);
            this.app.map.map.removeLayer(this.fixedLine);
            this.fixedLine = null;
            btn.textContent = btn.dataset.title1;
        }
    }

    showTree(tree, isFirstInv) {
        this.treeForm.show(tree, isFirstInv);
    }

    showTreeInStepTreeList(tree, index, tbody) {
        const props = tree.properties;
        const klasses = props.tree == '' ? ['new'] : [];
        if (props.marked) klasses.push('marked');
        // TODO: use inheritance to replace this if/else
        if (props.spec) {
            insertTableRow(tbody, '#tree_row', klasses, ['', tree.speciesName(), tree.diameter()]);
        } else {
            let dbh = tree.diameter();
            if (dbh > 0 && props.prev_dbh) {
                let diff = props.dbh - props.prev_dbh;
                let sign = diff >= 0 ? '+' : '';
                dbh = props.dbh + ' (' + sign + diff + ')';
            }
            insertTableRow(
                tbody, '#tree_row', klasses, [
                    props.nr, tree.speciesName(),
                    props.distance.toString().replace('.00', ''),
                    activeAngleUnit() == 'gon' ? props.azimuth : Math.round(props.azimuth * 0.9),
                    dbh, props.vita[1]
                ]
            );
        }
        tbody.lastElementChild.treeObject = tree;
        // Show the delete icon for new trees.
        if (klasses.includes('new')) {
            let img = tbody.lastElementChild.querySelector("img");
            img.addEventListener('click', async (ev) => {
                ev.stopPropagation();  // Avoid displaying the tree
                var resp = await modal.confirm(getTrans('treedelconfirm'));
                if (resp == 'ok') {
                    tbody.removeChild(ev.target.closest('tr'));
                    tree.plotobs.removeTree(tree);
                }
            });
        }
    }

    async speciesFromCaliper(specCode, specChars) {
        // Check if current form is a tree form, and if yes, if it is valid,
        // submit it, else alert a new tree cannot be measured if the current
        // form is not valid.
        const activeForm = this.getActive();
        const spSelect = document.getElementById(this.speciesSelectId);
        if (activeForm && activeForm.id == 'steptree') {
            const speciesChanged = (spSelect.value != '' && spSelect.options[spSelect.selectedIndex].dataset.abrev != specChars);
            const alreadyDBH = this.treeForm.diameterInput.value != ''
            if (speciesChanged || alreadyDBH) {
                beep_error();
                let resp = await modal.confirm(
                    getTrans('treeform_open'), getTrans('yes_replace'), getTrans('no_cancel')
                );
                if (resp != 'ok') return false;
            }
        } else {
            // Start a new tree form
            this.showTreeStep(null)
        }

        // Set species to the tree form
        let specOpt = document.querySelector(`[data-abrev='${specChars}']`);
        if (!specOpt) {
            beep_error();
            modal.alert(getTrans('badspeciesfromcaliper').replace('${specChars}', specChars));
        } else {
            spSelect.value = specOpt.value;
            beep_success();
        }
        return true;
    }
    async diameterFromCaliper(mm) {
        // Set diameter to the tree form (floored in cm)
        this.treeForm.diameterInput.value = Math.floor(mm / 10);
    }
    async distanceFromCaliper(cm) {
        // Set distance to the tree form (rounded in dm)
        const dist = Math.round(cm / 10);
        // Check distance if already present
        if (this.treeForm.distanceInput.value != '' && (Math.abs(parseInt(this.treeForm.distanceInput.value) - dist) > 3)) {
            let resp = await modal.confirm(
                getTrans('dist_mismatch'), getTrans('yes_replace'), getTrans('no_cancel')
            );
            if (resp != 'ok') return false;
        }
        this.treeForm.distanceInput.value = dist;
    }

    /* Return the active form */
    getActive() {
        for (var i=0; i < this.forms.length; i++) {
            if (this.forms[i].style.display == 'block') return this.forms[i];
        }
        return null;
    }
}

function computeEdgeFactor(radius, mDist) {
    const h = radius - mDist;
    let surfTot = radius * radius * Math.PI;
    let edgef = radius * radius * Math.acos(1 - h/radius) - (radius - h) * Math.sqrt(2*radius*h - h*h);
    return rounded(1 - edgef / surfTot, 1);
}
