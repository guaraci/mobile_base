import { getTrans } from './strings.js';

var audio = null;

export function beep(vol, freq, duration) {
  if (audio === null) audio = new AudioContext();
  if (audio._busy === true) {
      setTimeout(beep, 50, vol, freq, duration);
      return;
  }
  var v = audio.createOscillator();
  var u = audio.createGain();
  v.connect(u);
  v.frequency.value = freq;
  v.type = "square";
  u.connect(audio.destination);
  u.gain.value = vol*0.01;
  v.start();
  audio._busy = true;
  v.stop(audio.currentTime + duration * 0.001);
  setTimeout(() => audio._busy = false, duration);
}

export function beep_success() {
    beep(100, 700, 250);
}

export function beep_error() {
    beep(100, 120, 500);
}

export function isOnline() {
    return window.navigator.onLine;
}

export function isArray(variable) {
    return Object.prototype.toString.call(variable) === '[object Array]';
}

export function isBlank(form) {
    // FIXME: not valid for radio values
    for (var i=0; i < form.elements.length; i++) {
        if (!form.elements[i].name.length) continue;
        if (form.elements[i].value != '') return false;
    }
    return true;
}

export function rounded(num, dec) {
    return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
}

/*
 * Use the data-toggleimg img attribute to toggle its src image.
 * `img`: either the element itself or an ID.
 */
export function toggleImg(img) {
    if (!(img instanceof HTMLElement)) {
        img = document.getElementById(img);
    }
    const newSrc = img.dataset.toggleimg;
    img.dataset.toggleimg = img.src;
    img.src = newSrc;
}

export function totalHeight(elemId) {
    var style = document.defaultView.getComputedStyle(document.getElementById(elemId), '');
    var result = parseInt(style.getPropertyValue('height'), 10) + parseInt(style.getPropertyValue('margin-top'), 10) + parseInt(style.getPropertyValue('margin-bottom'), 10);
    if (isNaN(result)) return 0;
    return result;
}

export function loadJSON(path) {
    return fetch(path).then(response => {
        if (response.url.indexOf("/login/") >= 0) {
            window.location.reload(true);
        }
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject({
                status: response.status,
                statusText: response.statusText
            });
        }
    });
}

export function onEachFeature(feature, layer) {
    // Display property content for features
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
    }
}

export function getRadioValue(radioName) {
    for (const inp of document.querySelectorAll('input[name=' + radioName + ']')) {
        if (inp.checked) return inp.value;
    }
}

export function gradToRad(angle) {
     return angle * Math.PI / 200;
}

export function gradToDeg(angle) {
    return 360 * angle / 400;
}

export function degToGrad(angle) {
  return 400 * angle / 360;
}

export function degToRad(angle) {
  return angle * (Math.PI / 180);
}

export function insertTableRow(tbody, templateSel, klasses, values) {
    let template = document.querySelector(templateSel);
    let row = template.content.cloneNode(true);
    klasses.forEach(kl => row.firstElementChild.classList.add(kl));
    var tds = row.querySelectorAll("td");
    values.forEach((val, idx) => {
        tds[idx].textContent = val;
    });
    tbody.appendChild(row);
    return row;
}

export function activeAngleUnit() {
    const angleInput = document.querySelector('input[name="angleunit"]:checked') || document.querySelector('input[name="angleunit"]');
    return angleInput.value;
}

/* Return the difference in meters/azimuth between two projected coordinates (unit in meters. rounded to cm). */
export function computeCoordDifference(coords1, coords2) {
    const latDiff = coords2[1] - coords1[1];
    const longDiff = coords2[0] - coords1[0];
    // Pythagore
    const dist = Math.round(Math.sqrt(Math.abs(latDiff) * Math.abs(latDiff) + Math.abs(longDiff) * Math.abs(longDiff)) * 100) / 100;
    const degAngle = (450 - Math.floor(Math.atan2(latDiff, longDiff) * 180 / Math.PI)) % 360;
    const angle = {
        value: (activeAngleUnit() == 'gon' ? Math.round(degToGrad(degAngle)) : degAngle),
        symbol: (activeAngleUnit() == 'gon' ? 'ᵍ' : '°')
    };
    return {distance: dist, angle: angle};
}

export function getPlotObsFormSteps() {
    // -2: remove tree and final steps
    return document.querySelectorAll('.input-form').length - 2;
}

export function setFormDefaultFromProperties(form, props) {
    for (var i = 0; i < form.elements.length; i++) {
        // Don't touch unnamed inputs or inputs from formset management forms
        let element = form.elements[i];
        if (!element.name.length || element.name.indexOf('_FORMS') >= 0) continue;
        if (element.tagName == 'SELECT') {
            if (props[element.name]) {
                element.value = props[element.name][0];
            } else {
                for (var j = 0; j < element.options.length; j++) {
                    if (j > 0 && element.options[j].defaultSelected) {
                        element.value = element.options[j].value;
                        break;
                    }
                }
            }
        } else {
            if (props[element.name]) {
                if (element.type == 'radio') {
                    element.checked = element.value == props[element.name][0];
                } else if (element.type == 'checkbox') {
                    element.checked = true;
                } else {
                    element.value = props[element.name];
                }
            } else {
                if ((element.type == 'radio' || element.type == 'checkbox') && element.defaultChecked === true) {
                    element.checked = true;
                }
            }
        }
    }
}

// Debug function to show cache keys
export function show_cache_keys(cacheName) {
    caches.open(cacheName).then(cache => {
        cache.keys().then(keys => {
             for (let i=0; i < keys.length; i++) {
                 console.log(keys[i].url);
             }
        });
    });
}

export function redirectLogMessages() {
    const origLog = console.log;
    const logger = document.querySelector('#debug-messages');
    console.log = (message) => {
        if (typeof message == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br>';
        } else {
            logger.innerHTML += message + '<br>';
        }
        origLog(message);
    }
}

export const epsg2056String = "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs"
export var epsg2056Converter = proj4(
    proj4('EPSG:4326'),
    epsg2056String
);


class Modal {
    constructor() {
        this.loading = document.querySelector('#loading');
        this.modalDiv = document.querySelector('#modal-div');
    }

    showWaiting() {
        this.loading.classList.remove('hidden');
        this._show();
    }

    showHelp(target) {
        this.modalDiv.querySelector(`#${target}`).classList.remove('hidden');
        this._show();
    }

    alert(message) {
        this.modalDiv.querySelector('.message').textContent = message;
        this.modalDiv.querySelector('#buttons-confirm').classList.add('hidden');
        this.modalDiv.querySelector('#buttons-alert').classList.remove('hidden');
        this.modalDiv.querySelector('#modal-confirm').classList.remove('hidden');
        const okBtn = this.modalDiv.querySelector('#alert-ok');
        const promise = new Promise((resolve, reject) => {
            var listener = event => {
                okBtn.removeEventListener('click', listener);
                this.hide();
                resolve(true);
            };
            okBtn.addEventListener('click', listener);
        });
        this._show(true);
        return promise;
    }

    confirm(message, yesLabel='OK', noLabel=getTrans('cancel')) {
        this.modalDiv.querySelector('.message').textContent = message;
        this.modalDiv.querySelector('.modal-close').classList.add('hidden');
        this.modalDiv.querySelector('#modal-confirm').classList.remove('hidden');
        this.modalDiv.querySelector('#buttons-confirm').classList.remove('hidden');
        this.modalDiv.querySelector('#buttons-alert').classList.add('hidden');
        const btns = this.modalDiv.querySelector('#modal-confirm').querySelectorAll('button');
        // Create a promise waiting for a button click to be resolved.
        const promise = new Promise((resolve, reject) => {
            var listener = event => {
                btns.forEach(btn => btn.removeEventListener('click', listener));
                this.hide();
                resolve(event.target.value);
            };
            btns.forEach(btn => {
                btn.addEventListener('click', listener);
                if (btn.value == 'ok') btn.textContent = yesLabel;
                if (btn.value == 'cancel') btn.textContent = noLabel;
            });
        });
        this._show(true);
        return promise;
    }

    _show(narrow) {
        if (narrow === true) this.modalDiv.classList.add('narrow');
        else this.modalDiv.classList.remove('narrow');
        this.modalDiv.classList.remove('hidden'); }

    hide() {
        this.loading.classList.add('hidden');
        this.modalDiv.classList.add('hidden');
        this.modalDiv.querySelector('.modal-close').classList.remove('hidden');
        this.modalDiv.querySelectorAll('.help').forEach((div) => div.classList.add('hidden'));
        this.modalDiv.querySelector('#modal-confirm').classList.add('hidden');
    }
}

export const modal = new Modal();
