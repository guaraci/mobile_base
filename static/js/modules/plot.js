import { getTrans } from './strings.js';
import { isArray, isOnline, loadJSON, modal } from './utils.js';
import { getCounter } from './counter.js';
import { TreeObs } from './treeobs.js';

const thisYear = new Date().getFullYear();
const STEP_FINISHED = 'finished';


export class PlotObs {
    constructor(plot, data) {
        this.trees = [];
        this._can_have_trees = true;
        this.plot = plot;
        if (data) {
            // When data is provided, it is a GeoJSON structure.
            // The first feature is the center coords with PlotObs properties
            this.id = data.features[0].id;
            this.plot_id = data.plot;
            this.year = data.features[0].properties.year;
            this.geometry = data.features[0].geometry;
            this._properties = data.features[0].properties;
            // Following features are trees
            function dataEmpty(props) {
                return (props.diameter=='' && props.perim=='');
            }
            for (var i = 1; i < data.features.length; i++) {
                var treeData = data.features[i];
                if (treeData.validate === false && dataEmpty(treeData.properties)) continue;
                this.trees.push(new TreeObs(this, data.features[i]));
            }
            if (this.trees.length > 0 && this.trees[0].properties.azimuth) {
                this.trees.sort((a, b) => a.properties.azimuth - b.properties.azimuth);
            }
            if (data.fillstep) this.fillStep = data.fillstep;
            else this.fillStep = STEP_FINISHED;
        } else {
            this.plot_id = plot.id;
            this._properties = {type: "center"};
            this.geometry = plot.center();
            this.fillStep = "1";
        }
    }

    asGeoJSON() {
        // First feature is the Plot center with the PlotObs properties.
        // All following features are trees.
        var struct = {
            type: "FeatureCollection",
            plot: this.plot_id,
            fillstep: this.fillStep,
            features: [{
                type: "Feature",
                geometry: this.geometry,
                properties: this._properties
            }]
        };
        for (var i=0; i < this.trees.length; i++) {
            struct.features.push(this.trees[i].asGeoJSON());
        }
        return struct;
    }

    dbId() {
        return 'plotobs-' + this.plot_id + '-' + this.year;
    }

    treeById(id) {
        return this.trees.filter(tree => tree._id == id)[0];
    }

    properties() {
        var result = {};
        for (var prop in this._properties) {
            // Exclude non-interesting props or data already shown at plot level
            if (['type', 'Aufnahmejahr', 'Gemeinde', 'Aufnahmepunkt (plot)'].indexOf(prop) >= 0) {
                continue;
            }
            result[prop] = this._properties[prop];
        }
        return result;
    }

    setProperty(name, value) {
        this._properties[name] = value;
        if (name == 'year') {
            this.year = value;
        }
        // When forest edge factor is lower than 0.6, no trees are observed.
        // FIXME: add ability to revert if value is changed again
        if (name == 'forest_edgef' && parseFloat(value) < 0.6) {
            this.trees = [];
            this._can_have_trees = false;
            this.plot.app.map.removePointsLayer('treeLayer');
            this.plot.app.map.removePointsLayer('ghostLayer');
            // Remove allstepselect entries for trees
            var stepSelect = document.getElementById('allstepsselect');
            for (var i = stepSelect.length - 1; i >= 0; i--) {
                if (stepSelect.options[i].text.includes(getTrans('tree'))) {
                    stepSelect.removeChild(stepSelect.options[i]);
                }
            }
            document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'none';
        }
    }

    addTreeObs(previousTree) {
        var tree = new TreeObs(this);
        if (previousTree) {
            // Copy all properties from previous treeObs except dbh
            tree.setCoords(previousTree.getCoords());
            tree.properties = JSON.parse(JSON.stringify(previousTree.properties)); // Deep copy
            delete tree.properties.obs;
            tree.properties.prev_dbh = tree.properties.dbh;
            tree.properties.dbh = '';
        }
        this.trees.push(tree);
        return tree;
    }

    removeTree(tree) {
        tree.removeFromMap();
        this.trees.splice(this.trees.indexOf(tree), 1);
    }

    notCutTrees() {
        return this.trees.filter(item => item.properties.vita && !item.properties.vita[1].includes("(c)"));
    }

    getNextAvailableNumber() {
        let number = 1;
        for (var i = 0; i < this.trees.length; i++) {
            const nr = parseInt(this.trees[i].properties['nr']);
            if (nr >= number) {
                number = nr + 1;
            }
        }
        return number;
    }

    setUpStepSelect() {
        const stepSelect = document.getElementById('allstepsselect');
        // Reset step select to its initial state
        if (!stepSelect.dataset.initial) {
            stepSelect.dataset.initial = stepSelect.innerHTML;
        } else {
            stepSelect.innerHTML = stepSelect.dataset.initial;
        }
        document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'block';
        // Disable future steps (depending on current fillStep value)
        let activate = true;
        for (var i=0; i < stepSelect.options.length; i++) {
            if (activate) stepSelect.options[i].removeAttribute("disabled");
            else stepSelect.options[i].setAttribute("disabled", "disabled");
            if (this.fillStep == stepSelect.options[i].value) activate = false;
        }
    }

    showDetails() {
        this.showTrees(this.plot.app.map, false);
        document.getElementById('obs-input').style.display = 'none';
        document.getElementById('error').style.display = 'none';
        document.getElementById('button-' + this.year).classList.add("current");

        // Show plotobs data
        var generalTable = document.getElementById('obs-infos-general-table');
        generalTable.innerHTML = "";
        var obsProperties = this.properties();
        for (var prop in obsProperties) {
            var tr = generalTable.insertRow();
            tr.id = `info-${prop}`;
            var td = tr.insertCell();
            var verbose_prop = verbose_names[prop];
            if (!verbose_prop) {
                var label = document.querySelector(`label[for="id_${prop}"]`);
                if (label) verbose_prop = label.textContent.replace(/:$/, '').trim();
                else verbose_prop = prop;
            }
            td.appendChild(document.createTextNode(verbose_prop));
            td = tr.insertCell();
            var value = obsProperties[prop];
            if (isArray(value)) {
                value = value[1]; // first is object pk, second is human-readable value
            } else if (value === true || value === false) {
                value = getTrans('yesno')[value];
            }
            td.appendChild(document.createTextNode(value));
        }
        // Show tree data
        var treeTable = document.getElementById('obs-infos-trees-table');
        treeTable.innerHTML = "";
        this.trees.forEach((tree) => {
            if (tree.validated) tree.insertInTreeTable(treeTable);
        });
        // Show data
        document.getElementById('obs-data').style.display = 'block';
        document.getElementById('obs-infos-general').classList.remove('hidden');
        document.querySelector('[data-target="obs-infos-general"]').classList.add('active');
        document.getElementById('obs-infos-trees').classList.add('hidden');
        document.querySelector('[data-target="obs-infos-trees"]').classList.remove('active');
    }

    showTrees(map, showNumbers=true) {
        if (this.trees.length && this.trees[0].properties.azimuth !== undefined) {
            map.removePointsLayer('treeLayer');
            map.removePointsLayer('ghostLayer');
            if (!this.isFinished()) {
                this.trees.forEach((tree) => { tree.showOnMap(map, 'ghostLayer', showNumbers); });
            } else {
                if (map.vLayers.treeLayer) map.vLayers.treeLayer.clearLayers();
                this.trees.forEach((tree) => { tree.showOnMap(map, 'treeLayer', showNumbers); });
            }
        }
    }

    allTreesValidated() {
        let unvalidated = 0;
        this.trees.forEach((tree) => {
            if (!tree.validated) unvalidated += 1;
        });
        return unvalidated === 0;
    }

    async readyForSubmit() {
        if (!this.allTreesValidated()) {
            await modal.alert(getTrans('trees_unvalidated'));
            return false;
        }
        // No trees? ask for confirmation
        if (this.trees.length < 1 && this._can_have_trees) {
            var resp = await modal.confirm(getTrans('notrees'));
            if (resp != 'ok') return false;
        }
        return true;
    }

    stepFilled(step) {
        const stepSelect = document.getElementById('allstepsselect');
        var steps = Array.from(stepSelect.options).map(opt => opt.value);
        if ((steps.indexOf(step) + 1) > steps.indexOf(this.fillStep)) {
            this.fillStep = steps[steps.indexOf(step) + 1];
        }
    }

    isFinished() {
        return this.fillStep == STEP_FINISHED;
    }

    async finish() {
        this.fillStep = STEP_FINISHED;
        var self = this;
        var success = true;
        if (isOnline()) {
            try {
                await this.sendToServer();
            } catch(err) {
                var msg = getTrans('servererr').replace('ERROR', err.statusText);
                if (err.responseText) msg += `\n${getTrans('details')}: ${err.responseText}`;
                modal.alert(msg);
                self.saveLocally();
                success = false;
            } finally {
                // force icon refresh as its color may change
                self.plot.showOnMap();
            }
        } else {
            this.saveLocally();
        }
        return success;
    }

    saveLocally() {
        const db = this.plot.app.db;
        const id = this.dbId();
        this.plot.app.saveData(id, this.asGeoJSON());
        // Save the id for future sync
        db.get('new-plotobs').catch(err => {
            if (err.name === 'not_found') {
                return {
                    _id: 'new-plotobs',
                    newIDs: []
                };
            }
        }).then(doc => {
            if (doc.newIDs.indexOf(id) < 0) {
                doc.newIDs.push(id);
                getCounter().increment();
                return db.put(doc);
            }
        });
        // force icon refresh as its color may change
        if (this.plot) this.plot.showOnMap();
    }

    remove() {
        // Remove an unfinished PlotObs (no UI for this function yet)
        // TODO: change color of year button and plot point
        if (this.isFinished()) {
            modal.alert("Unable to remove a completed observation.");
            //alert("Impossible de supprimer une observation terminée.");
            return;
        }
        const docId = this.dbId();
        const db = this.plot.app.db;
        db.get(docId).then(doc => {
            getCounter().decrement();
            // Remove the id from the newIDs list
            db.get('new-plotobs').then(doc => {
                doc.newIDs.splice(doc.newIDs.indexOf(docId), 1);
                db.put(doc);
            });
            return db.remove(doc);
        }).catch(err => {
            modal.alert("An error occurred while trying to remove the current observation: " + err.statusText);
            //alert("Une erreur s'est produite en essayant de supprimer l'observation actuelle: " + err.statusText);
        }).then(() => {
            delete this.plot.app.currentPlot.obs[thisYear];
            this.plot.app.currentPlot._years.shift();
            var prevObs = this.plot.app.currentPlot.previousObs(thisYear);
            if (prevObs) {
                document.getElementById("button-" + prevObs.year).click();
            }
        });
    }

    sendToServer(saveLocal=true) {
        // Send data to server
        var self = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", sync_url);
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    if (this.responseURL.indexOf('/login/') >= 0) {
                        reject({status: 403, responseText: "Not logged in the platform."});
                    } else {
                        resolve(self);
                    }
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText,
                        responseText: this.responseText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            var postData = JSON.stringify(self.asGeoJSON());
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.send(postData);
        }).then(() => {
            const counter = getCounter()
            counter.decrement();
            // Remove the id from the newIDs list
            const db = counter.db;
            db.get('new-plotobs').then(doc => {
                doc.newIDs.splice(doc.newIDs.indexOf(self.dbId()), 1);
                db.put(doc);
            });
        }).catch(err => {
            if (err.status > 299) {
                // This will allow reediting the PlotObs.
                self.fillStep = 'final';
            }
            if (self.plot && err.responseText && err.responseText.includes("Some previous trees are missing")) {
                const prevObs = self.plot.previousObs(self.year);
                const currentTreeIds = self.trees.map(t => t.properties.tree);
                prevObs.notCutTrees().forEach((tree) => {
                    // Check and re-add missing tree to obs.
                    if (!currentTreeIds.includes(tree.properties.tree.toString())) {
                        self.addTreeObs(tree);
                        tree.showOnMap(self.plot.app.map, 'treeLayer', true);
                    }
                });
                // refresh tree list
                self.plot.app.formSet.showTreeListStep(self);
            }
            if (saveLocal) self.saveLocally();
            throw err;
        });
    }
}


export class Plot {
    constructor(app, feature) {
        this.app = app;
        this.id = feature.id;
        this.geoJSON = feature;
        this.properties = feature.properties;
        this.obs = {};
        this.layer = null;
        let arr = Object.keys(this.geoJSON.properties.obsURLs);
        this._years = arr.map(obj => parseInt(obj));
    }
    center() { return this.geoJSON.geometry; }

    years() {
        // Return existing PlotObs years, sorted from more recent to older.
        this._years.sort();
        this._years.reverse();
        return this._years;
    }

    fillYearButtonsDiv() {
        var buttonDiv = document.getElementById('year-buttons'),
            years = this.years().slice(),
            hasNewYear = false,
            self = this;
        if (years.indexOf(thisYear) < 0) {
            // Add "this" year if not existing, in red
            years.unshift(thisYear);
            hasNewYear = true;
        }
        for (var i=0; i<years.length; i++) {
            var button = document.createElement("button"),
                year = years[i];
            button.setAttribute('id', 'button-' + year);
            button.className = "year";
            if (year == thisYear) {
                if (hasNewYear) button.classList.add("new");
                else if (!self.obs[year].isFinished()) button.classList.add("editing");
            }
            button.innerHTML = year;
            button.addEventListener('click', (ev) => {
                const btn = ev.target;
                if (btn.className.indexOf('current') > -1) return;
                const oldCurrent = document.querySelector("button.current");
                if (oldCurrent) oldCurrent.classList.remove("current");
                btn.classList.add("current");
                var obs = self.obs[parseInt(btn.innerText)];
                if (obs === undefined || !obs.isFinished()) {
                    this.app.formSet.startInput(obs);
                } else {
                    obs.showDetails();
                }
            });
            buttonDiv.appendChild(button);
        }
    }

    hasObs(year) { return this.years().indexOf(year) >= 0; }

    getObs(year) {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self.obs[year]) resolve(self.obs[year]);
            else if (self.properties.obsURLs[year] !== undefined) {
                loadJSON(self.properties.obsURLs[year]).then(data => {
                    var po = new PlotObs(self, data);
                    self.setObs(po, year);
                    resolve(po);
                }).catch(err => {
                    console.log(err);
                    reject(new Error(getTrans('notavailoffline')));
                });
            } else {
                // Check if locally saved
                self.app.db.get('plotobs-' + self.id + '-' + year).then(doc => {
                    var po = new PlotObs(self, doc);
                    self.setObs(po, year);
                    resolve(po);
                }).catch(err => {
                    reject(new Error(getTrans('nodataforyear') + year));
                });
            }
        });
    }

    setObs(po, year) {
        this.obs[year] = po;
        if (this._years.indexOf(year) < 0) this._years.push(year);
    }

    getLatestObs() {
        // Return the more recent finished PlotObs
        var proms = [];
        for (let i = 0; i < this._years.length; i++) {
            proms.push(this.getObs(this.years()[i]));
        }
        return Promise.all(proms).then(obss => {
            for (let i = 0; i < obss.length; i++) {
                if (obss[i].isFinished()) return obss[i];
            }
        });
    }

    previousObs(year) {
        var previousIdx = this.years().indexOf(parseInt(year)) + 1;
        // We suppose the previous obs has been loaded (should be, but...)
        if (previousIdx < this.years().length) return this.obs[this.years()[previousIdx]];
        else return null;
    }

    createObs(year) {
        let po = new PlotObs(this, null);
        po.setProperty('year', year);
        this.obs[year] = po;
        this._years.unshift(year);

        var previousObs = this.previousObs(year);
        if (previousObs) {
            var trees = previousObs.notCutTrees();
            // Create a copy of each uncut tree from previous obs
            for (var i=0; i < trees.length; i++) {
                po.addTreeObs(trees[i]);
            }
        }
        return po;
    }

    showOnMap(map) {
        map = map || this.app.map;
        if (this.layer) {
            map.plotLayer.removeLayer(this.layer);
        }
        var icon = L.divIcon({className: 'icon-blue', iconSize: [20, 20], iconAnchor: [10, 10]});
        // Show different icon color if obs for this year is already done (or partially done)
        if (this.hasObs(thisYear)) {
            if (this.obs[thisYear] && !this.obs[thisYear].isFinished()) {
                icon = L.divIcon({className: 'icon-orange', iconSize: [20, 20], iconAnchor: [10, 10]});
            } else {
                icon = L.divIcon({className: 'icon-green', iconSize: [20, 20], iconAnchor: [10, 10]});
            }
        }
        var coords = this.center().coordinates.slice().reverse();
        this.layer = L.marker(coords, {icon: icon});
        var self = this;
        this.layer.on({click: (ev) => {
            map.focusOnPlot(coords, self.properties.radius);
            self.getLatestObs().then(obs => {
                self.showDetails();
                if (obs) obs.showDetails();
            }).catch(err => {
                // Display error
                document.getElementById('error').innerHTML = `Error: ${err.message}`;
                document.getElementById('error').style.display = 'block';
            });
        }});
        map.plotLayer.addLayer(this.layer);
    }

    showDetails() {
        document.getElementById('obs-input').style.display = 'none';
        document.getElementById('inventory-data').setAttribute('hidden', true);
        // Show general plot data under the map
        document.querySelector('#undermap-commune').textContent = this.properties.municipality_name;
        document.querySelector('#undermap-coords').innerHTML = '<span>' + this.properties.coords_2056[0] + '</span> <span>' + this.properties.coords_2056[1] + '</span>';
        if (this.properties.coords_2056_exact) {
            document.querySelector('#undermap-coords').innerHTML += '<span>' + this.properties.coords_2056_exact[0] + '</span> <span>' + this.properties.coords_2056_exact[1] + '</span>';
        }
        // Show plot data
        const plotDataDiv = document.querySelector('#plot-data');
        plotDataDiv.innerHTML = '';
        const template = document.querySelector('#plot-data-template');
        this.properties.plotData.forEach(data => {
            var fieldEmpty = this.properties[data.field] == '' || this.properties[data.field] == null;
            if (!data.showEmpty && fieldEmpty) return;
            var clone = template.content.cloneNode(true);
            clone.querySelector('div.label').textContent = data.label;
            clone.querySelector('div.value').textContent = fieldEmpty ? '-' : this.properties[data.field] + (data.unit || '');
            clone.querySelector('div.value').id = `plot-${data.field}`;
            plotDataDiv.append(clone);
        });
        document.getElementById('plot-data').removeAttribute('hidden');
        if (this != this.app.currentPlot) {
            this.app.currentPlot = this;
            this.fillYearButtonsDiv();
        }
        //if (navigator.geolocation) navigator.geolocation.getCurrentPosition(checkCurrentPointDifference);
    }
}
