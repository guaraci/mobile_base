'use strict';
import { computeCoordDifference, epsg2056Converter, loadJSON, modal, onEachFeature, rounded } from './utils.js';
import { getTrans } from './strings.js';

const DISTANCE_WARNING_METERS = 50;

export class Map {
    constructor(app, mapOptions) {
        this.app = app;
        this.mapStyles = {
            'BestandesKarte': {
                "color": "#ff7800",
                "weight": 1,
                "opacity": 0.65
            },
            'Erschliessung': {
                "color": "#ff0000",
                "weight": 2,
            }
        }
        this.zoomPlot = this.constructor.ZOOM_PLOT;
        this.map = L.map('map-div', mapOptions || {});
        this.vLayers = {};
        this.centerOptions = {
            radius: 3,
            fillColor: "#ff1100"
        };
        this.map.createPane('highlightPane');
        this.map.getPane('highlightPane').style.zIndex = 650;
        this.treeLayer = null;
        this.setupLayers();
        this.baseLayers.forEach(lay => lay.addTo(this.map));
        this._altBaseLayers = [this.orthoLayer];
        // Initially empty layers
        this.inventoryLayer = L.geoJSON().addTo(this.map);
        this.plotLayer = L.featureGroup().addTo(this.map);
        L.control.scale({imperial: false}).addTo(this.map);
        this.resetView();
    }

    resetView() {
        this.map.setView(this.constructor.CENTER_INITIAL, this.constructor.ZOOM_INITIAL);
    }

    setupLayers() { /* Custom client subclasses create their layers here. */}

    fitToLayer() {
        this.map.fitBounds(this.plotLayer.getBounds());
    }

    attachHandlers(db) {
        var self = this;
        this.map.on('moveend', () => {
            // Store current center and zoom level
            var zoom = self.map.getZoom(),
                center = self.map.getCenter();
            db.get('current-state').then(currentState => {
                var changed = false;
                if (currentState.zoom !== zoom) {
                    currentState.zoom = zoom;
                    changed = true;
                }
                if (!center.equals(currentState.center)) {
                    currentState.center = center;
                    changed = true;
                }
                if (changed) {
                    db.put(currentState).catch(err => {
                        // Ignoring any error, as this is not critical;
                        console.log("Non-fatal error: " + err);
                        return;
                    });
                }
            });
        });
        this.map.on('zoom', () => {
            if (this.canStoreOffline()) document.getElementById('sync-unsync').classList.remove('inactive');
            else document.getElementById('sync-unsync').classList.add('inactive');
        });
        // Display current position on the map
        this.longDiv = document.getElementById('currentLong');
        this.latDiv = document.getElementById('currentLat');
        this.accDiv = document.getElementById('accuracyValue');
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(position => {
                    self.setCurrentPosition(position);
                    checkCurrentPointDifference(position, this.app.currentPlot);
                }, error => {
                    console.log("Geolocation error: " + error.message);
                },
                {timeout: 10000, enableHighAccuracy: true, maximumAge: 5000
            });
        }
    }

    setCurrentPosition(position) {
        // Diff from my position in CdF (debugging help)
        var devDiff = [0, 0]; //position.coords.longitude < 7 ? [0.457, 0.85] : [0, 0];
        if (!this.currentPositionLayer) {
            this.currentPositionLayer = L.marker(
                [0, 0], {
                icon: L.icon({iconUrl: staticImages.currentPos, iconSize: [20, 20]}),
                opacity: 0.9
            });
            this.map.addLayer(this.currentPositionLayer);
        }
        this.currentPositionLayer.setLatLng([
            position.coords.latitude + devDiff[0],
            position.coords.longitude + devDiff[1]
        ]);
        var projected = epsg2056Converter.forward([
            position.coords.longitude + devDiff[1],
            position.coords.latitude + devDiff[0]]
        );
        this.longDiv.textContent = rounded(projected[0], 2);
        this.latDiv.textContent = rounded(projected[1], 2);
        let accuracy = rounded(position.coords.accuracy, 2);
        if (this.accDiv) this.accDiv.textContent = `${accuracy}m`;
    }

    switchBaseLayers() {
        this.baseLayers.forEach(lay => this.map.removeLayer(lay));
        let tempLayers = this._altBaseLayers;
        this._altBaseLayers = this.baseLayers;
        this.baseLayers = tempLayers;
        this.baseLayers.forEach(lay => lay.addTo(this.map));
    }

    toCurrentPosition() {
        if (this.currentPositionLayer) {
            var newPos = this.currentPositionLayer.getLatLng();
            this.map.setView(newPos, Math.max(this.map.getZoom(), 16));
        } else {
            modal.alert(getTrans("nocurloc"));
        }
    }

    resetLayers() {
        if (this.vLayers.treeLayer) {
            this.vLayers.treeLayer.clearLayers();
        }
        if (this.vLayers.plotCircle) {
            this.map.removeLayer(this.vLayers.plotCircle);
        }
        if (this.vLayers.plotCenter) {
            this.map.removeLayer(this.vLayers.plotCenter);
        }
        if (this.vLayers.ghostLayer) {
            this.map.removeLayer(this.vLayers.ghostLayer);
        }
    }

    unHighlight() {
        document.querySelectorAll('.icon-tree').forEach(icon => icon.classList.remove('highlight'));
    }

    removePointsLayer(layerType) {
        if (this.vLayers[layerType]) {
            this.map.removeLayer(this.vLayers[layerType]);
            this.vLayers[layerType] = null;
        }
    }

    loadLayer(layerName, url) {
        var self = this;
        loadJSON(url).then(geojson => {
            if (self.vLayers[layerName]) {
                self.vLayers[layerName].clearLayers();
                self.vLayers[layerName].addData(geojson);
            } else {
                self.vLayers[layerName] = L.geoJSON(geojson, {
                    style: geojson.mapStyle || self.mapStyles[layerName],
                    onEachFeature: onEachFeature
                });
                self.vLayers[layerName].addTo(self.map);
            }
        });
    };

    showInventory(geojson, fitMap) {
        this.inventoryLayer.clearLayers();
        // Add inventory data to layer
        this.inventoryLayer.addData(geojson);
        if (fitMap) this.map.fitBounds(this.inventoryLayer.getBounds());
    }

    resetToOverview() {
        // Reset map/info div sizes
        this.unHighlight();
        document.getElementById('map-container').classList.remove('focused');
        document.getElementById('infos-div').classList.remove('focused');
        document.getElementById('error').style.display = 'none';
        var self = this;
        setTimeout(() => { self.map.invalidateSize()}, 400);
        // Return to the inventory view
        if (!this.map.hasLayer(this.plotLayer)) this.map.addLayer(this.plotLayer);
        if (this.map._stored_map_state) {
            this.map.setView(this.map._stored_map_state[0], this.map._stored_map_state[1]);
        } else {
            this.map.fitBounds(this.plotLayer.getBounds());
        }
    }

    focusOnPlot(coords, radius) {
        this.map.removeLayer(this.plotLayer);
        this.vLayers.plotCircle = L.circle(coords, radius).addTo(this.map);
        this.vLayers.plotCenter = L.circle(coords, 0.1).addTo(this.map);
        // Make the map div smaller when focused on plot.
        document.getElementById('map-container').classList.add('focused');
        document.getElementById('infos-div').classList.add('focused');
        var self = this;
        setTimeout(() => { self.map.invalidateSize()}, 400);
        this.map._stored_map_state = [this.map.getCenter(), this.map.getZoom()];
        this.map.setView(coords, this.zoomPlot);
    }

    canStoreOffline() {
        const zoomCurrent = this.map.getZoom(),
              zoomMax = this.baseLayers[0].options.maxNativeZoom || this.baseLayers[0].options.maxZoom;
        if (zoomMax - zoomCurrent > 4) {
            return false;
        }
        return true;
    }

    /*
     * Store map data offline.
     * Vector data are already automatically cached through serviceworker.js.
     */
    storeOffline() {
        var self = this;
        this.progressBar = null;
        const bbox = this.map.getBounds(),
              zoomCurrent = this.map.getZoom(),
              zoomMax = this.baseLayers[0].options.maxNativeZoom || this.baseLayers[0].options.maxZoom;
        return new Promise(function(resolve, reject) {
            document.getElementById('progress').style.display = 'block';
            self.progressBar = new ProgressBar.Line('#progress', {
                color: '#008000', strokeWidth: 5
            });
            self.baseLayers[0].off('seedprogress').on('seedprogress', (seedData) => {
                //var percent = 100 - Math.floor(seedData.remainingLength / seedData.queueLength * 100);
                self.progressBar.set(self.progressBar.value() + (1 / seedData.queueLength));
            });
            self.baseLayers[0].off('seedend').on('seedend', (seedData) => {
                self.progressBar.destroy();
                self.progressBar = null;
                document.getElementById('progress').style.display = 'none';
                resolve(true);
            });

            /*
             * Some figures about tiles caching:
             * tile size: 256x256
             * tile download size: 25Kb
             * tile size in db: (Base64), ~220Kb??
             * on one map level: 12 tiles
             * seeding:
             *   - 1 level: 42/48 tiles (1.1Mb dl / 10Mb stor.)
             *   - 2 levels: 140/150 tiles (3.6Mb dl / 35Mb stor.)
             *   - 3 levels: ~500 tiles (12.5Mb dl / 120Mb stor.)
             *   - 4 levels: ~2000 tiles (50Mb dl / 500Mb stor.)
             * Browser Storage docs:
             *  https://medium.com/dev-channel/offline-storage-for-progressive-web-apps-70d52695513c#.tva434uxh
             */
            self.baseLayers[0].seed(bbox, zoomCurrent, zoomMax);
            // Progress/end of seeding is handled in 'seedprogress'/'seedend' events
        });
    }
}

function checkCurrentPointDifference(position, currentPlot) {
    if (currentPlot === null) return;
    const warningDiv = document.querySelector('#coords_warning');
    if (!warningDiv) return;
    var projected = epsg2056Converter.forward([
        position.coords.longitude,
        position.coords.latitude
    ]);
    var difference = Math.floor(computeCoordDifference(currentPlot.properties.coords_2056, projected));
    if (difference.distance > DISTANCE_WARNING_METERS) {
        warningDiv.classList.remove('hidden');
    } else {
        warningDiv.classList.add('hidden');
    }
}

export function checkLonLatDifference(plot) {
    // Check and warn if difference between theoretical and real coordinate is too high
    var theo_longVal = plot.properties.coords_2056[0];
    var theo_latVal = plot.properties.coords_2056[1];
    var longVal = document.getElementById('id_exact_long').value;
    var latVal = document.getElementById('id_exact_lat').value;
    document.getElementById('exact_long_warning').style.display = 'none';
    document.getElementById('exact_lat_warning').style.display = 'none';
    if (longVal != '' && Math.abs(longVal - theo_longVal) > 20) {
        document.getElementById('exact_long_warning').innerHTML = getTrans('coorddiff') + Math.floor(Math.abs(longVal - theo_longVal)) + 'm';
        document.getElementById('exact_long_warning').style.display = 'block';
    }
    if (latVal != '' && Math.abs(latVal - theo_latVal) > 20) {
        document.getElementById('exact_lat_warning').innerHTML = getTrans('coorddiff') + Math.floor(Math.abs(latVal - theo_latVal)) + 'm';
        document.getElementById('exact_lat_warning').style.display = 'block';
    }
}
