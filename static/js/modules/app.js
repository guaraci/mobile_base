import { Plot, PlotObs } from './plot.js';
import { isBlank, isOnline, loadJSON, modal, redirectLogMessages, toggleImg, totalHeight } from './utils.js';
import { getTrans } from './strings.js';
import { getCounter, initCounter } from './counter.js';

const thisYear = new Date().getFullYear();


export class MobileApp {
    version = '0.9.72';

    constructor() {
        this.currentPlot = null;
        this.angleUnit = 'gon';
        this.db = new PouchDB('ksp', {adapter: 'idb'});
        this.plots = [];
        this.map = null;
    }

    /* Only called when DOM is ready. */
    init() {
        redirectLogMessages();
        this.loadInventoryChoices();
        // Init the counter with non-null db
        initCounter(this.db);
        this.attachDOMhandlers();
        this.formSet.initHandlers();
        this.checkOfflineData();
    }

    initMap(mapOptions) {
        if (this.map == null) {
            this.map = new this.mapClass(this, mapOptions);
            this.map.attachHandlers(this.db);
        }
    }

    loadInventoryChoices() {
        document.getElementById('main-container').style.display = 'none';
        const invSelect = document.querySelector('#inventory-choice');
        fetch(invSelect.dataset.url).then(resp => {
            const contentType = resp.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                return resp.json().then(data => {
                    data.forEach(item => {
                        const opt = document.createElement('option');
                        opt.innerHTML = item.name;
                        opt.value = item.pk;
                        opt.dataset.url = item.url;
                        invSelect.appendChild(opt);
                    });
                });
            } else {
                // Typically, the user is not connected
                location.href = login_url;
            }
        });
    }

    /* Called when going back from obs view to overview. */
    resetInfos() {
        document.getElementById('plot-data').setAttribute('hidden', true);
        document.getElementById('year-buttons').innerHTML = "";
        document.getElementById('obs-data').style.display = 'none';
        document.getElementById('obs-input').style.display = 'none';
        document.getElementById('inventory-data').removeAttribute('hidden');
        this.updateInventoryData();
        this.formSet.hideAll();
        this.map.resetLayers();
        this.currentPlot = null;
    }

    goingOnline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "block";
        this.checkOfflineData();
    }

    goingOffline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "none";
        //db.allDocs({startkey: 'gemeinde-', endkey: 'gemeinde-zzz'}).then (function (result) {
            // TODO: disable municipalities without offline content in the select list
        //});
    }

    async checkOfflineData() {
        // If unsynced content, propose to sync
        try {
            var doc = await this.db.get('new-plotobs');
        } catch (err) {
            // A not_found indicates we have no unsynced content
            if (err.name !== 'not_found') {
                console.log(err);
            }
            return;
        }
        if (doc.newIDs.length == 0) return;

        var promises = [];
        // Get all locally saved ids
        for (var i=0; i < doc.newIDs.length; i++) {
            var docId = doc.newIDs[i];
            var p = this.db.get(docId).then(podata => {
                return new PlotObs(null, podata);
            }).catch(err => {
                console.log(`Unable to retrieve local PlotObs with id ${docId} (${err})`);
            });
            promises.push(p);
        }
        var pos = await Promise.all(promises);
        pos = pos.filter(po => po !== undefined); // undefined in case of errors
        if (!pos.length) return;
        var finishedObs = pos.filter(po => po.isFinished());
        if (!finishedObs.length) return;
        let resp = await modal.confirm(getTrans('syncconfirm'), getTrans('yesno')[true], getTrans('yesno')[false]);
        if (resp != 'ok') return;
        var sendPromises = [];
        for (var i=0; i < finishedObs.length; i++) {
            var p = finishedObs[i].sendToServer(false);
            sendPromises.push(p);
        }
        Promise.all(sendPromises).then(() => {
            modal.alert(getTrans('datasaved'));
        }).catch(err => {
            modal.alert(getTrans('datasavederr'));
        });
    }

    /*
     * After the user choose the inventory, restore the map state to the previous
     * position (if the inventory did not change) or to the inventory overview.
     */
    setInitialState(inventoryOption) {
        // Read state stored in the db (current municipality) and restore it
        this.db.get('current-state').then(data => {
            return data;
        }).catch(err => {
            if (err.name === 'not_found') {
                console.log("No current-state existing document, creating a new one.");
                var newState = {_id: 'current-state'};
                var newProm = this.db.put(newState).then(res => {
                    return {_id: res.id, _rev: res.rev};
                }).catch(err => {
                    console.log("Unable to save a new current-state object: " + err);
                });
                return newProm;
            } else throw err;
        }).then(currentState => {
            document.getElementById('inventory-title').innerHTML = inventoryOption.text;
            if (currentState.inventory && currentState.inventory == inventoryOption.value) {
                this.loadInventoryData(inventoryOption);
                // We are still in the same inventory, just restore the state
            } else if (inventoryOption.value) {
                currentState.inventory = inventoryOption.value;
                this.db.put(currentState).then(() => {
                    // set a new inventory overview
                    this.loadInventoryData(inventoryOption);
                }).catch(err => {
                    console.log("Unable to save current-state with new inventory id: " + err);
                });
            } else {
                this.map.resetView();
            }
        });
    }

    /*
     * Load inventory data: inventory boundaries, plots, wms layers.
     */
    loadInventoryData(option) {
        const self = this;
        self.initMap();
        return loadJSON(option.dataset.url).then((inventory) => {
            self.prepareInventory(inventory);
            document.querySelector('#main-container').dataset.inventorytype = inventory.properties.type;
            return new Promise(function(resolve, reject) {
                modal.showWaiting();
                resolve(loadJSON(inventory.properties.plotsURL));
                // This can go on asynchronously
                self.map.showInventory(inventory, true);
                Object.keys(inventory.properties.layerURLs).forEach((k, i) => {
                    self.map.loadLayer(k, inventory.properties.layerURLs[k]);
                });
            });
        }).then(features => {
            // Fetch locally saved PlotObs, if any
            return new Promise(function(resolve, reject) {
                self.db.get('new-plotobs').then((doc) => { resolve([features, doc.newIDs]); }
                ).catch((err) => { resolve([features, []]); });
            });
        }).then(result => {
            var features = result[0],
                newIds = result[1],
                plotIdsWithNew = {}; // Array storing locally-saved PlotObs
            newIds.map(obj => {
                plotIdsWithNew[parseInt(obj.split('-')[1])] = parseInt(obj.split('-')[2]);
            });
            self.plots = [];
            let promises = [];
            for (let i = 0; i < features.features.length; i ++) {
                let plot = new Plot(self, features.features[i]);
                if (plotIdsWithNew.hasOwnProperty(plot.id)) {
                    promises.push(
                        plot.getObs(plotIdsWithNew[plot.id]).then(po => {
                            po.plot.showOnMap(self.map);
                        })
                    );
                } else plot.showOnMap(self.map);
                self.plots.push(plot);
            }
            document.querySelector('#total_plot_num').textContent = self.plots.length;
            Promise.all(promises).then(() => {
                self.map.fitToLayer();
                self.updateInventoryData();
            });
            modal.hide();
        });
    }

    forceLoadPlotObs() {
        var promises = [];
        this.plots.forEach((plot) => {
            // Call every PlotObs URL to get them in the cache
            // FIXME: Limit to visible plots
            for (var j = 0, years = plot.years(); j < years.length; j++) {
                promises.push(plot.getObs(years[j]));
            }
        });
        return Promise.all(promises);
    }

    prepareInventory(inventory) {}

    updateInventoryData() {
        let inventoried = 0,
            notInventoried = 0,
            curInventoried = 0;
        this.plots.forEach((plot) => {
            if (plot.hasObs(thisYear)) {
                if (plot.obs[thisYear] && !plot.obs[thisYear].isFinished()) {
                    curInventoried += 1;
                } else inventoried += 1;
            } else notInventoried += 1;
        });
        document.querySelector('#plot_inventoried_num').textContent = inventoried;
        document.querySelector('#plot_inworks_num').textContent = curInventoried;
        document.querySelector('#plot_not_inventoried_num').textContent = notInventoried;
    }

    currentObs() {
        return this.currentPlot.obs[thisYear];
    }

    goToStep(step) {
        // Validate currently-visible form. We don't really care if the form
        // pass validation or not. If not, the values are not saved and the tree
        // not marked as validated.
        this.formSet.validateForm(null);
        this.formSet.showStep(step);
    }

    unlockInput(ev) {
        const lockImg = ev.target;
        if (lockImg.src.includes('unlocked')) return;
        toggleImg(lockImg);
        var prevElement = lockImg.previousElementSibling;
        while(prevElement !== null) {
            if (prevElement.disabled) {
                prevElement.disabled = false;
                break;
            }
            prevElement = prevElement.previousElementSibling;
        }
    }

    tabClick(ev) {
        document.querySelectorAll('.tab-child').forEach((child) => {
            child.classList.add('hidden');
        });
        const tab = ev.target;
        document.getElementById(tab.dataset.target).classList.remove('hidden');
        var tabDiv = document.getElementById('obs-tabs');
        tabDiv.querySelector("li.active").className = "";
        tab.className = "active";
        // Show tree numbers on the map only when the active tab is "trees"
        const treeIcons = document.getElementsByClassName('icon-tree');
        if (tab.dataset.target == 'obs-infos-trees') {
            for(let i = 0; i < treeIcons.length; i++) { treeIcons[i].classList.remove('nonumber'); }
        } else {
            this.map.unHighlight();
            for(let i = 0; i < treeIcons.length; i++) { treeIcons[i].classList.add('nonumber'); }
        }
    }

    forceOrientation() {
        // if current orient is landscape, force portrait
        const isLandscape = document.getElementById('map-container').offsetTop == document.getElementById('infos-div').offsetTop;
        if (isLandscape) {
            document.querySelector('body').classList.remove('force-landscape');
            document.querySelector('body').classList.add('force-portrait');
        } else {
            document.querySelector('body').classList.remove('force-portrait');
            document.querySelector('body').classList.add('force-landscape');
        }
    }

    attachDOMhandlers() {
        const self = this;
        // Inventory selection form at application load
        document.getElementById('inventory-form').addEventListener('submit', (ev) => {
            ev.preventDefault();
            const inventorySelector = ev.target.elements[0];
            if (inventorySelector.selectedIndex < 1) {
                modal.alert(getTrans('chooseinv'));
                return;
            }
            document.getElementById('inventory-choice-div').style.display = 'none';
            document.getElementById('main-container').style.display = 'flex';
            const angleUnit = document.querySelector('input[name="angleunit"]:checked');
            if (angleUnit) {
                this.angleUnit = angleUnit.value;
                document.cookie = `angleunit=${angleUnit.value}; SameSite=Strict; Secure`;
            }
            this.setInitialState(inventorySelector[inventorySelector.selectedIndex]);
        });
        // Tab handling
        document.querySelectorAll('ul.tabs li').forEach((tab) => {
            tab.addEventListener('click', this.tabClick.bind(this));
        });
        // Base layer switcher
        document.getElementById('baselayer').addEventListener('click', (ev) => {
            toggleImg('baselayer');
            self.map.switchBaseLayers();
        });
        document.getElementById('tocurrent').addEventListener('click', (ev) => {
            self.map.toCurrentPosition();
        });
        document.getElementById('toinventory').addEventListener('click', (ev) => {
            self.map.fitToLayer();
        });
        document.getElementById('force-orient').addEventListener('click', (ev) => {
            self.forceOrientation();
        });
        document.getElementById('sync-unsync').addEventListener('click', async (ev) => {
            if (!self.map.canStoreOffline()) {
                modal.alert(getTrans("zonetoobig"));
                return;
            }
            var resp = await modal.confirm(getTrans('localsave'));
            if (resp != 'ok') return;
            var inventorySelect = document.getElementById('inventory-choice'),
                inventoryOption = inventorySelect[inventorySelect.selectedIndex];
            try {
                await Promise.all([
                    self.forceLoadPlotObs(),
                    self.map.storeOffline()
                ]);
                modal.alert(getTrans("datasavedlocal"));
            } catch(err) {
                modal.alert(err);
            }
        });
        // Start/End observation buttons
        document.getElementById('input-start').addEventListener('click', (ev) => {
            document.getElementById('step0').style.display = 'none';
            this.formSet.initForms(null);
        });
        document.getElementById('input-end').addEventListener('click', async (ev) => {
            // If the current form is filled, validate it
            const form = this.formSet.getActive();
            if (!isBlank(form)) {
                const form_valid = this.formSet.validateForm(form);
                if (!form_valid) return;
            }
            const obs = this.currentObs();
            var resp = await obs.readyForSubmit();
            if (!resp) return;
            modal.confirm(getTrans('finalconfirm')).then(async resp => {
                if (resp != 'ok') return;
                let success = await obs.finish();
                if (success) {
                    this.formSet.hideAll();
                    obs.showDetails();
                }
            });
        });

        // Select widget giving access to steps
        document.getElementById('allstepsselect').addEventListener('change', (ev) => {
            self.goToStep(ev.target.value);
        });
        // Input unlock buttons
        document.querySelectorAll('img.locked-input').forEach((lock) => {
            lock.addEventListener('click', this.unlockInput.bind(this));
        });
        // Back to overview button
        document.getElementById('gemeinde-back').addEventListener('click', (ev) => {
            self.resetInfos();
            self.map.resetToOverview();
        });

        const versionDiv = document.getElementById('appversion');
        versionDiv.textContent = 'v.' + this.version;
        versionDiv.addEventListener('dblclick', (ev) => {
            document.querySelector('#debug-pane').classList.remove('hidden');
        });
        const connectedDiv = document.getElementById('connectedas');
        connectedDiv.addEventListener('dblclick', (ev) => {
            if (isOnline()) location.href = logout_url + '?next=/';
            else modal.alert(getTrans('discoffline'));
        });
        const reloader = document.querySelector('#debug-reload');
        reloader.addEventListener('click', (ev) => {
            window.location.reload(true);
        });

        // Form help buttons
        document.querySelectorAll('.form-help').forEach((img) => {
            img.addEventListener('click', (ev) => modal.showHelp(img.dataset.target));
        });
        document.querySelectorAll('.modal-close').forEach((button) => {
            button.addEventListener('click', (ev) => {
                modal.hide();
                if (ev.target.dataset.target) {
                    document.querySelector('#' + ev.target.dataset.target).classList.add('hidden');
                }
            });
        });

        if (!window.navigator.onLine) toggleImg('network-status');
        window.addEventListener('online',  this.goingOnline.bind(this));
        window.addEventListener('offline', this.goingOffline.bind(this));
    }

    saveData(docId, data) {
        // Save data in local database, replacing any existing occurrences
        this.db.get(docId).then(doc => {
            return this.db.remove(doc);
        }).then(result => {
            data._id = docId;
            return this.db.put(data);
        }).catch(err => {
            if (err.name === 'not_found') {
                data._id = docId;
                return this.db.put(data);
            } else throw err;
        });
    }

    // Debug function to clear locally saved data
    clear_local_data() {
        this.db.get('new-plotobs').catch(err => {
            return;
        }).then(doc => {
            doc.newIDs = [];
            return this.db.put(doc);
        });
        getCounter().setCount(0);
    }
}
