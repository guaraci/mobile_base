import { activeAngleUnit, degToRad, gradToRad, setFormDefaultFromProperties } from './utils.js';
import { getTrans } from './strings.js';

/*
 * TreeObs class
 */
export class TreeObs {
    constructor(plotobs, data) {
        this.plotobs = plotobs;
        this.treePoint = null;  // the marker for the map.
        // Incremental id (1..n) unique per plotobs.
        this._id = Math.max(...(this.plotobs.trees.map(tree => tree._id).concat([0]))) + 1;
        this._coords = null;
        if (data) {
            // Data is a GeoJSON structure received from a query on existing PlotObs
            this.validated = !(data.validated === false);
            this.properties = data.properties;
            if (data.geometry && data.geometry.coordinates) {
                this._coords = data.geometry.coordinates;
            }
        } else {
            this.validated = false;
            this.properties = {};
        }
    }

    app() {
        return this.plotobs.plot.app;
    }

    setProperty(name, value) {
        this.properties[name] = value;
        if (name == 'azimuth' || name == 'distance') {
            this._coords = null; // Invalidate calculated coords
        }
    }

    getProperty(name) {
        return this.properties[this.app().formSet.treeFields[name] || name];
    }

    diameter() {
        return this.getProperty('diameter');
    }

    speciesName() {
        return this.getProperty('species')[1];
    }

    isCut() {
        if (!this.properties.vita) return false;
        let vitaStr = this.properties.vita[1];
        return (vitaStr.includes("(c)") || vitaStr.includes("(x)") || vitaStr.includes("(y)"));
    }

    isDead() {
        if (!this.properties.vita) return false;
        let vitaStr = this.properties.vita[1];
        return (vitaStr.includes("(m)") || vitaStr.includes("(z)"));
    }

    getPreviousTreeObs() {
        var prevPlotObs = this.plotobs.plot.previousObs(this.plotobs.year);
        if (prevPlotObs) {
            for (var i = 0; i < prevPlotObs.trees.length; i++) {
                if (prevPlotObs.trees[i].properties.tree == this.properties.tree) return prevPlotObs.trees[i];
            }
        }
        return null;
    }

    getCoords(reverse) {
        // Return lon, lat (as geoJSON), unless `reverse` is true
        if (!this._coords && this.properties.azimuth) {
            // Calculate new coords based on distance (m) and azimuth (grad)
            var plotCoords = this.plotobs.geometry.coordinates;
            var distance = parseFloat(this.properties.distance);
            var delta_lon = (distance / 10) * Math.sin(gradToRad(this.properties.azimuth)) / (111320 *Math.cos(degToRad(plotCoords[1])));
            var delta_lat = (distance / 10) * Math.cos(gradToRad(this.properties.azimuth)) / 110540;
            this._coords = [
                plotCoords[0] + delta_lon,
                plotCoords[1] + delta_lat
            ];
        }
        if (reverse && this._coords) return this._coords.slice().reverse();
        else return this._coords;
    }

    setCoords(coords) {
        this._coords = coords.slice();
    }

    asGeoJSON() {
        return {
            type: "Feature",
            geometry: this._coords ? {type: "Point", coordinates: this.getCoords()} : {},
            validated: this.validated,
            properties: this.properties
        };
    }

    showOnMap(map, parentLayerName, showNumbers=false) {
        var layerOpts = {},
            size = 20,
            treeIcon = document.querySelector('#icon-tree').cloneNode(true),
            coords = this.getCoords(true),
            self = this;
        if (!coords || isNaN(coords[0])) return; // No available coords yet for this point
        if (!map.vLayers[parentLayerName]) {
            map.vLayers[parentLayerName] = L.layerGroup();
            map.map.addLayer(map.vLayers[parentLayerName]);
        }
        if (!this.validated) {
            var prevTObs = this.getPreviousTreeObs();
            if (prevTObs && prevTObs.properties.dbh == 0) {
                // Special case where a living tree was wrongly marked with dbh=0
                treeIcon = document.querySelector('#icon-tree-special').cloneNode(true);
            } else {
                treeIcon = document.querySelector('#icon-tree-todo').cloneNode(true);
            }
        } else {
            size = Math.floor(13 + ((Math.abs(this.properties.dbh) - 11) / 4));
            if (this.isCut()) {
                treeIcon = document.querySelector('#icon-tree-cut').cloneNode(true);
            } else if (this.isDead()) {
                treeIcon = document.querySelector('#icon-tree-dead').cloneNode(true);
            }
        }
        const treeNum = this.properties.nr === undefined ? '' : this.properties.nr;
        treeIcon.id += treeNum;
        if (this.treePoint !== null && map.vLayers['ghostLayer']) {
            map.vLayers['ghostLayer'].removeLayer(this.treePoint);
        }
        const classes = this.getProperty('marked') ? ['icon-tree', 'marked'] : ['icon-tree'];
        if (!showNumbers) classes.push('nonumber');
        treeIcon.querySelector('text').textContent = treeNum;
        if (treeIcon.classList.contains('living_tree') && this.getProperty('color')) {
            let circle = treeIcon.querySelector('circle');
            let style = circle.getAttribute("style");
            circle.setAttribute("style", style.replace(/(fill:)#\w+;/, `$1${this.getProperty('color')};`));
        }
        this.treePoint = L.marker(coords, {
            icon: L.divIcon({
                className: classes.join(' '),
                iconSize: [size, size],
                html: treeIcon.outerHTML
            }),
            opacity: 0.9
        });
        this.treePoint.on({click: self.makeCurrent.bind(self)});
        map.vLayers[parentLayerName].addLayer(this.treePoint);
    }

    removeFromMap() {
        if (this.treePoint) {
            // Is there a better way to detect parent layer?
            if (this.app().map.vLayers.ghostLayer) this.app().map.vLayers.ghostLayer.removeLayer(this.treePoint);
            if (this.app().map.vLayers.treeLayer) this.app().map.vLayers.treeLayer.removeLayer(this.treePoint);
        }
    }

    /* If editing, go to proper step.
     * Else, highlight point and line in tree list.
     */
    makeCurrent() {
        if (!this.plotobs.isFinished()) {
            if (this.app().formSet.allStepEnabled()) this.app().formSet.showTreeStep(this);
        } else if (this.properties.nr) {
            const treeTable = document.getElementById('obs-infos-trees-table');
            if (treeTable.offsetParent !== null) {
                const current = treeTable.getElementsByClassName("current").item(0);
                if (current) current.classList.remove("current");
                document.getElementById('treeline-' + this.properties.nr).classList.add("current");
                this.highlightOnMap();
            }
        }
    }

    highlightOnMap() {
        this.app().map.unHighlight();
        this.treePoint._icon.classList.add('highlight');
    }

    /**
     * Insert the current TreeObs instance as a new line in @table.
     */
    insertInTreeTable(table) {
        var tr = table.insertRow();
        if (this.getProperty('marked')) tr.classList.add('marked');
        if (this.getProperty('color') && !this.isCut() && !this.isDead()) {
            tr.setAttribute("style", `background-color: ${this.getProperty('color')};`);
        }
        if (typeof this.properties.nr !== "undefined") {
            tr.setAttribute("id", "treeline-" + this.properties.nr);
            var td = tr.insertCell();
            td.appendChild(document.createTextNode(this.properties.nr + '.'));
        }
        var td = tr.insertCell();
        td.appendChild(document.createTextNode(this.speciesName()));
        var td = tr.insertCell();
        var azimuth = null;
        if (this.properties.azimuth !== undefined) {
            azimuth = activeAngleUnit() == 'gon' ? this.properties.azimuth : Math.round(this.properties.azimuth * 0.9);
        }
        td.appendChild(document.createTextNode(
            '⌀' + this.diameter() + (azimuth ? '/' + azimuth : '') + (this.getProperty('distance') ? '/' + this.getProperty('distance') : '')
        ));
        if (this.properties.vita) {
            var td = tr.insertCell();
            td.appendChild(document.createTextNode(this.properties.vita[1]));
        }
        tr.addEventListener('click', this.makeCurrent.bind(this));
    }
}


/*
 * Form instance to edit/add a tree obs.
 */
export class TreeForm {
    constructor(formSet) {
        this.formSet = formSet;
        this.app = formSet.app;
        this.form = document.getElementById('steptree');
        this.vitaSelect = document.getElementById('id_vita');
        this.diameterInput = document.getElementById(`id_${this.formSet.treeFields.diameter}`);
        this.distanceInput = document.getElementById(`id_${this.formSet.treeFields.distance}`);
        this.azimuthInput = document.getElementById(`id_${this.formSet.treeFields.azimuth}`);
    }

    show(tree, isFirstInv) {
        this.tree = tree;
        this.reset();

        var isNewTree = tree === null || tree.properties.tree == '' || tree.properties.tree === undefined;
        if (isNewTree) {
            if (this.vitaSelect) {
                // Only let empty, 'e' (if not first inventory), 'l', 'm' or 'z' as enabled vita choices
                for (var i = 0; i < this.vitaSelect.length; i++) {
                    var opt = this.vitaSelect[i];
                    if (i == 0 || (!isFirstInv && opt.text.includes('(e)')) || opt.text.includes('(l)')
                               || opt.text.includes('(m)') || opt.text.includes('(z)')) {
                        opt.disabled = false;
                    } else opt.disabled = true;
                }
            }
        } else {
            if (this.vitaSelect) {
                // Disable vita 'e' option
                for (var i = 0; i < this.vitaSelect.length; i++) {
                    if (this.vitaSelect[i].text.includes('(e)'))
                        this.vitaSelect[i].disabled = true;
                    else this.vitaSelect[i].disabled = false;
                }
            }
            // Inform widget of previous dbh value
            const prevTObs = tree.getPreviousTreeObs();
            if (prevTObs) {
                this.diameterInput.dataset.previousValue = prevTObs.properties.dbh;
            }
            this.diameterInput.focus();
        }

        let map = this.app.map;
        if (tree === null) {
            // Entering a blank new tree form
            map.unHighlight();
            if (this.vitaSelect) {
                // Enable 'e' or 'l' vita select choice by default
                var defaultVita = isFirstInv ? "(l)" : "(e)";
                for (var i = 0; i < this.vitaSelect.length; i++) {
                    if (this.vitaSelect[i].text.includes(defaultVita))
                        this.vitaSelect.selectedIndex = i;
                }
                const obs = this.app.currentObs();
                document.getElementById('id_nr').value = obs.getNextAvailableNumber();
            }
        } else {
            // Populate form from tree properties
            setFormDefaultFromProperties(this.form, tree.properties);
            if (activeAngleUnit() == 'deg') {
                this.form.elements.id_azimuth.value = Math.round(parseInt(this.form.elements.id_azimuth.value) * 0.9);
            }
            if (tree.properties.tree != '' && !tree.validated && tree.properties.vita && !tree.properties.vita[1].includes('(m)')) {
                // Force vita to 'l' (unless it was dead (m) before)
                for (var i = 0; i < this.vitaSelect.length; i++) {
                    if (this.vitaSelect[i].text.indexOf("(l)") >= 0) {
                        this.vitaSelect.selectedIndex = i;
                        break;
                    }
                }
            }
            tree.highlightOnMap();
        }
        this.hideHistoricTrees();
        this.checkStemHeight();
        this.setValuesFromVita();
    }

    reset() {
        this.formSet.resetForm(this.form);
        if (document.getElementById('azimuth-unit')) {
            // KSP-specific
            this.form.elements.id_tree.value = '';
            document.getElementById('azimuth-unit').innerHTML = this.app.angleUnit;
            this.distanceInput.max = this.app.currentPlot.properties.radius * 10;
            this.azimuthInput.max = this.app.angleUnit == 'gon' ? 400 : 360;
        }
        this.form.querySelectorAll("[id$='_warning']").forEach(el => el.style.display = 'none');
        this.diameterInput.removeAttribute("disabled");
        if (!this.diameterInput.min) this.diameterInput.setAttribute("min", "12");
        this.diameterInput.dataset.previousValue = null;
        // Undo hideHistoricTrees
        const specSelect = document.querySelector('#id_spec, #id_species');
        for (let j = 0; j < specSelect.options.length; j++) {
            specSelect.options[j].style.display = 'block';
        }
    }

    hideHistoricTrees() {
        // Hide all 'historic' species unless selected
        const specSelect = document.querySelector('#id_spec, #id_species');
        let warn = false;
        for (var j = 0; j < specSelect.options.length; j++) {
            if (specSelect.options[j].dataset.historic == 'true') {
                if (j == specSelect.selectedIndex) {
                    warn = true;
                    document.getElementById('species_warning').style.display = 'block';
                } else {
                    specSelect.options[j].style.display = 'none';
                }
            }
        }
        if (!warn) document.getElementById('species_warning').style.display = 'none';
    }

    checkStemHeight() {
        const stemH = document.getElementById('id_stem_height');
        if (!stemH) return;
        if (stemH.value != '') {
            document.getElementById('stem_height_warning').style.display = 'none';
            return;
        }
        const vita = document.getElementById('id_vita');
        const vitaCode = vita.options[vita.selectedIndex].text.substr(-2, 1);
        const stem = document.getElementById('id_stem');
        const stemCode = stem ? stem.options[stem.selectedIndex].text.substr(-2, 1) : '';

        if (vitaCode == 'z' || vitaCode == 'm' || (stemCode && stemCode != 'B')) {
            document.getElementById('stem_height_warning').style.display = 'block';
        } else {
            document.getElementById('stem_height_warning').style.display = 'none';
        }
    }

    /*
     * Method called when the vita (Lebenslauf) select changes or is initialized
     * with a new tree.
    */
    setValuesFromVita() {
        if (!this.vitaSelect) return;
        const vitaCode = this.vitaSelect.options[this.vitaSelect.selectedIndex].text.substr(-2, 1);
        // A cut tree has dbh forced to 0
        if (vitaCode == 'c' || vitaCode == 'x' || vitaCode == 'y') {
            this.diameterInput.setAttribute("min", "0");
            this.diameterInput.value = '0';
            this.diameterInput.setAttribute("disabled", "disabled");
        } else {
            this.diameterInput.removeAttribute("disabled");
            this.diameterInput.setAttribute("min", "12");
        }
        // For cut, unfound or outside trees, select values are set to '' and disabled
        const selects = Array.from(document.getElementById('steptree').elements).filter(
            el => el.tagName == 'SELECT' && !['id_species', 'id_vita'].includes(el.id)
        );
        var nullBooleans = ['stem_forked', 'woodpecker'];
        if (vitaCode == 'c' || vitaCode == 'x' || vitaCode == 'y') {
            for (let i = 0; i < selects.length; i++) {
                selects[i].value = '';
                selects[i].setAttribute("disabled", "disabled");
            }
            for (let i = 0; i < nullBooleans.length; i++) {
                let nullB_0 = document.getElementById('id_' + nullBooleans[i] + '_0');
                if (nullB_0) {
                    nullB_0.checked = true;
                    nullB_0.setAttribute("disabled", "disabled");
                    document.getElementById('id_' + nullBooleans[i] + '_1').setAttribute("disabled", "disabled");
                    document.getElementById('id_' + nullBooleans[i] + '_2').setAttribute("disabled", "disabled");
                }
            }
        } else {
            for (let i = 0; i < selects.length; i++) {
                selects[i].removeAttribute("disabled");
            }
            for (let i = 0; i < nullBooleans.length; i++) {
                let nullB_0 = document.getElementById('id_' + nullBooleans[i] + '_0');
                if (nullB_0) {
                    nullB_0.removeAttribute("disabled");
                    document.getElementById('id_' + nullBooleans[i] + '_1').removeAttribute("disabled");
                    document.getElementById('id_' + nullBooleans[i] + '_2').removeAttribute("disabled");
                }
            }
        }
        // For dead trees, only some selects are disabled
        if (vitaCode == 'm' || vitaCode == 'z') {
            var m_selects = ['layer', 'rank', 'crown_form', 'crown_length'];
            for (let i = 0; i < m_selects.length; i++) {
                let select = document.getElementById('id_' + m_selects[i]);
                if (select) {
                    select.value = '';
                    select.setAttribute("disabled", "disabled");
                }
            }
            // Set value from previous obs if available
            if (this.diameterInput.value == '') {
                this.diameterInput.value = this.diameterInput.dataset.previousValue;
            }
        }
    }

    initHandlers(app) {
        const self = this;
        var showTreeOnMap = (name, val) => {
            if (self.tree) {
                self.tree.setProperty(name, val);
                self.tree.plotobs.showTrees(app.map);
                self.tree.highlightOnMap();
            }
        };
        var dbhCheck = (inp) => {
            const warnEl = document.getElementById('dbh_warning');
            if (!warnEl) return;
            if (inp.dataset.previousValue && parseInt(inp.value) <= inp.dataset.previousValue) {
                warnEl.style.display = 'block';
            } else {
                warnEl.style.display = 'none';
            }
        }

        if (this.azimuthInput) {
            this.distanceInput.addEventListener('input', (ev) => {
                showTreeOnMap('distance', ev.target.value);
            });
            this.azimuthInput.addEventListener('input', (ev) => {
                const val = TreeForm.convertAzimuth(ev.target.value);
                showTreeOnMap('azimuth', val);
            });
        }

        if (this.diameterInput) {
            this.diameterInput.addEventListener('input', (ev) => {
                dbhCheck(self.diameterInput);
            });
        }

        document.getElementById('id_perim').addEventListener('input', (ev) => {
            const diam = Math.round(ev.target.value / Math.PI);
            self.diameterInput.value = diam;
            dbhCheck(self.diameterInput);
        });
        document.getElementById('canceltree').addEventListener('click', (ev) => {
            // Cancelling a tree form.
            ev.preventDefault();
            const currentTree = app.formSet.treeForm.tree;
            if (self.tree && !self.tree.properties.tree) {
                // Remove new tree from obs.trees if it was added
                const treeArray = self.tree.plotobs.trees;
                const index = treeArray.indexOf(self.tree);
                if (index > -1) {
                    self.tree.plotobs.removeTree(self.tree);
                }
            }
            app.formSet.showStep(app.formSet.FINAL_STEP);
        });
        if (this.vitaSelect) {
            this.vitaSelect.addEventListener('change', (ev) => {
                self.checkStemHeight();
                self.setValuesFromVita();
            });
            const stem_input = document.getElementById('id_stem')
            if (stem_input) {
                document.getElementById('id_stem').addEventListener('change', (ev) => {
                    self.checkStemHeight();
                });
                document.getElementById('id_stem_height').addEventListener('change', (ev) => {
                    self.checkStemHeight();
                });
            }
        }
        const speechInputs = document.querySelectorAll('.speechEnabled');
        if (speechInputs && 'webkitSpeechRecognition' in window) {
            var recognition = new webkitSpeechRecognition();
            var currentInput = null;
            recognition.continuous = false;
            recognition.interimResults = false;
            recognition.onresult = function(ev) {
                console.log(ev);
                recognition.stop();
                let val = parseInt(event.results[0][0].transcript);
                if (!isNaN(val)) currentInput.value = val;
            }
            speechInputs.forEach(inp => {
                inp.addEventListener('focus', ev => {
                    currentInput = inp;
                    try {
                        recognition.start();
                    } catch(err) {
                        console.error(err);
                    }
                });
            });
        }
    }

    static convertAzimuth(value) {
        if (activeAngleUnit() == 'gon') return value;
        return Math.round(value / 0.9);
    };

}
