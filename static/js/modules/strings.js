const translations = {
    badspeciesfromcaliper: {
        fr: "Le code '${specChars}' reçu de la pince est inconnu",
        de: "Der vom Messschieber erhaltene Code '${specChars}' ist unbekannt"
    },
    cancel: {
        fr: "Annuler",
        de: "Abbrechen"
    },
    chooseinv: {
        fr: "Veuillez choisir un inventaire",
        de: "Wählen Sie bitte eine Aufnahme"
    },
    coorddiff: {
        fr: "La différence entre la coordonnée théorique et la coordonnée réelle est de : ",
        de: "Der Unterschied zwischen theoretischer und realer Koordinate ist: "
    },
    datasaved: {
        fr: "Les données sont maintenant enregistrées sur le serveur.",
        de: "Die Daten sind jetzt auf dem Server gespeichert."
    },
    datasavederr: {
        fr: "Le serveur n’a pas pu accepter toutes les données, veuillez réessayer plus tard.",
        de: "Der Server konnte nicht alle Daten akzeptieren, bitte versuchen Sie es später erneut."
    },
    datasavedlocal: {
        fr: "Les données ont bien été enregistrées sur votre appareil.",
        de: "Daten wurden erfolgreich gespeichert."
    },
    details: {
        fr: "Détails",
        de: "Details"
    },
    discoffline: {
        fr: "Impossible de se déconnecter en mode hors ligne.",
        de: "Abmelden im Offline-Modus nicht möglich."
    },
    dist_mismatch: {
        fr: "La distance diffère de plus de 30cm de celle qui était enregistrée. Voulez-vous vraiment remplacer avec la nouvelle distance mesurée ?",
        de: "Die Distanz weicht um mehr als 30cm von der erwarteten Distanz ab. Möchten sie die neu gemessene Distanz übernehmen?"
    },
    finalconfirm: {
        fr: "Souhaitez-vous terminer définitivement la saisie des informations de cette placette ?",
        de: "Möchten Sie die Eingabe der Informationen für diese Stichprobe endgültig abschließen?"
    },
    localsave: {
        fr: "Voulez-vous enregistrer localement les données et cartes visibles?",
        de: "Möchten sie jetzt auf der Karte sichtbaren Daten lokal speichern?"
    },
    newtree: {
        fr: "nouvel arbre",
        de: "neuer Baum"
    },
    no_cancel: {
        fr: "Non, annuler",
        de: "Nein, abbrechen"
    },
    nocurloc: {
        fr: "Votre emplacement n’a pas pu être déterminé.",
        de: "Ihr Standort konnte leider nicht ermittelt werden."
    },
    nodataforyear: {
        fr: "Aucune donnée pour l'année ",
        de: "Keine Daten für Jahr "
    },
    notavailoffline: {
        fr: "Les données de cette placette ne sont malheureusement pas disponibles hors ligne.",
        de: "Die Daten für diese Stichprobe sind leider nicht offline verfügbar."
    },
    notrees: {
        fr: "Aucun arbre n’a été saisi. Voulez-vous vraiment terminer cette placette ?",
        de: "Es wurden keine Bäume eingegeben. Möchten Sie diese Aufnahme wirklich abschließen?",
    },
    resyncconfirm: {
        fr: "Voulez-vous vraiment resynchroniser toutes les données locales ?",
        de: "Möchten sie wirklich alle lokalen Daten neu synchronisieren?"
    },
    servererr: {
        fr: "Les données d’inventaire n’ont pas pu être enregistrées sur le serveur. Elles ont été enregistrées localement. (L’erreur est: ERROR)",
        de: "Die aufnahmedaten konnten nicht auf dem Server gespeichert werden. Sie werden lokale gespeichern. (Fehler ist: ERROR)"
    },
    step: {
        fr: "Étape",
        de: "Schritt"
    },
    syncconfirm: {
        fr: "Souhaitez-vous synchroniser maintenant les nouvelles données hors ligne ?",
        de: "Möchten sie jetzt die neue offline Daten synchronisieren?"
    },
    syncpoints: {
        fr: " points ont été re-synchronisés.",
        de: " Punkte wurden neu synchronisiert."
    },
    tree: {
        fr: "Arbre",
        de: "Baum"
    },
    treedelconfirm: {
        fr: "Voulez-vous vraiment supprimer cet arbre ?",
        de: "Möchtest du diesen Baum wirklich löschen?"
    },
    treeform_open: {
        fr: "Un formulaire d’arbre est déjà ouvert. Voulez-vous remplacer les valeurs actuelles du formulaires par celles de la pince ?",
        de: "Ein Baumformular ist bereits geöffnet. Wollen Sie die aktuellen Werte des Formulars durch die des Kluppers ersetzen?"
    },
    trees_unvalidated: {
        fr: "Certains arbres ne sont pas encore validés.",
        de: "Einige Bäume sind noch nicht validiert."
    },
    yesno: {
        fr: {true: 'oui', false: 'non'},
        de: {true: 'ja', false: 'nein'}
    },
    yes_replace: {
        fr: "Oui, remplacer",
        de: "Ja, ersetzen"
    },
    zonetoobig: {
        fr: "Vous devez sélectionner une zone plus petite pour enregistrer la carte localement.",
        de: "Sie müssen einen kleineren Bereich auswählen um die Karte Daten lokale zu speichern."
    }
}

function getCurLang() {
    let lang = navigator.language.slice(0, 2);
    if (lang != 'fr' && lang != 'de') lang = 'de';
    return lang
}

export function getTrans(key) {
    return translations[key][getCurLang()];
}
