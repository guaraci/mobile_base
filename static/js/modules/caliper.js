import { beep_success } from './utils.js';

export class Caliper {
    constructor(formSet) {
        this.device = null;
        // UUIDs must be lowercase!
        this.serviceUuid = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
        // Receive from device
        this.txCharUuid = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';
        // Send to device
        this.rxCharUuid = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
        document.querySelector('button#caliper-disconnect').addEventListener('click', this.disconnect.bind(this));
        document.querySelector('button#caliper-connect').addEventListener('click', this.connect.bind(this));
        this.formSet = formSet;
        this.acceptValues = false;
        this.waitingValues = {};
        this.lastUnhandledValue = '';
    }

    showStatus() {
        if (this.device === null) {
            document.querySelector('#caliper-disconnected').removeAttribute('hidden');
            document.querySelector('#caliper-connected').setAttribute('hidden', true);
        } else {
            document.querySelector('#caliper-disconnected').setAttribute('hidden', true);
            document.querySelector('#caliper-connected').removeAttribute('hidden');
        }
    }

    connect(ev) {
        if (! navigator.bluetooth) {
            alert("Désolé, ce navigateur ne gère pas les connexions Bluetooth.");
            return;
        }
        console.log('Start connecting to Bluetooth device');
        var caliper = this;
        navigator.bluetooth.requestDevice({
            optionalServices: [caliper.serviceUuid],
            acceptAllDevices: true
        }).then(device => {
            var server = device.gatt.connect();
            caliper.device = device;
            device.addEventListener('gattserverdisconnected', caliper.onDisconnected.bind(caliper));
            caliper.showStatus();
            return server;
        }).then(server => {
            return server.getPrimaryService(caliper.serviceUuid);
        }).then(service => {
            service.getCharacteristic(caliper.txCharUuid).then(char => {
                char.addEventListener('characteristicvaluechanged', caliper.receiveValue.bind(caliper));
                char.startNotifications();
                caliper.txChar = char;
            }).catch(error => {
                console.log("Error getting tx characteristic: " + error);
            });
            service.getCharacteristic(caliper.rxCharUuid).then(char => {
                caliper.rxChar = char;
            }).catch(error => {
                console.log("Error getting rx characteristic: " + error);
            });
        }).catch(error => {
            console.log('Bluetooth error: ' + error);
            caliper.showStatus();
        });
    }

    disconnect(ev) {
        var caliper = this;
        this.txChar.stopNotifications().then(_ => {
            caliper.txChar.removeEventListener('characteristicvaluechanged', caliper.receiveValue);
            caliper.txChar == null;
        });
        this.device.gatt.disconnect();
        this.device = null;
        this.showStatus();
    }

    onDisconnected(event) {
        let device = event.target;
        console.log('Device ' + device.name + ' is disconnected.');
        this.device = null;
        this.showStatus();
    }

    async receiveValue(event) {
        // Data is sent as a NMEA string and ends with a checksum and end of line. [CR][LF]
        var rawValue = new Uint8Array(event.target.value.buffer);
        var value = String.fromCharCode.apply(null, rawValue).trim();
        // Acknowledgement expected from the caliper after a value has been sent (0x06).
        // If not sent within 1 second the MDII will post a message in the display saying “Check connection”
        this.rxChar.writeValue(Uint8Array.of(6));
        console.log('Value received by Bluetooth: ' + value);
        if (value.startsWith('$PHGF')) {
            // Start of new value line, discard any previous stored value
            this.lastUnhandledValue = '';
        }
        if (this.lastUnhandledValue && this.lastUnhandledValue.startsWith('$PHGF')) {
            value = this.lastUnhandledValue + value;
            this.lastUnhandledValue = ''
        }
        if (value.startsWith('$PHGF,SPC')) {
            // A new species value was received ($PHGF,SPC,2,ABC,*2B)
            let parts = value.split(',');
            let specCode = parts[2];
            let specChars = parts[3];
            this.waitingValues = {};
            // speciesFromCaliper can block on confirm window.
            this.acceptValues = false;  // Do not accept other values while waiting got speciesFromCaliper answer.
            this.acceptValues = await this.formSet.speciesFromCaliper(specCode, specChars);
            if (this.acceptValues) {
                if (this.waitingValues['diameter']) {
                    this.formSet.diameterFromCaliper(this.waitingValues['diameter']);
                    delete this.waitingValues['diameter'];
                }
                if (this.waitingValues['distance']) {
                    this.formSet.distanceFromCaliper(this.waitingValues['distance']);
                    delete this.waitingValues['distance'];
                }
            } else this.waitingValues = {};
        } else if (value.startsWith('$PHGF,DIA')) {
            // A new diameter(mm) value was received ($PHGF,DIA,M,277,*2A)
            let parts = value.split(',');
            let dia_mm = parts[3];
            if (this.acceptValues) {
                this.formSet.diameterFromCaliper(dia_mm);
                beep_success();
            } else this.waitingValues['diameter'] = dia_mm;
        } else if (value.startsWith('$PHGF,DSTDIA') && value.includes('*')) {
            // A combined diameter(mm)/distance(cm) value was received ($PHGF,DSTDIA,M,277,145,*38)
            let parts = value.split(',');
            let dia_mm = parseInt(parts[3]);
            let dist_cm = parseInt(parts[4]);
            if (this.acceptValues) {
                this.formSet.diameterFromCaliper(dia_mm);
                this.formSet.distanceFromCaliper(dist_cm);
                beep_success();
            } else {
                this.waitingValues['diameter'] = dia_mm;
                this.waitingValues['distance'] = dist_cm;
            }
        } else {
            this.lastUnhandledValue = value;
        }
    }

    fakeCaliper() {
        // For test usage
        if (this.txChar) return;  // Already setup
        this.txChar = document.createElement("div");
        this.txChar.id = 'fakeTxChar';
        this.txChar.value = {'buffer': 'ABCD'};
        this.txChar.setAttribute('hidden', true);
        document.body.appendChild(this.txChar);
        this.txChar.addEventListener('characteristicvaluechanged', this.receiveValue.bind(this));
        this.rxChar = { writeValue: function(v) { return; } };
    }
    sendFakeValue(value) {
        // For test usage
        this.txChar.value = {'buffer': Array.from(new TextEncoder().encode(value).values())};
        this.txChar.dispatchEvent(new Event('characteristicvaluechanged'));
    }
}
