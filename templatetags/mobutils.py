from pathlib import Path

from django import template
from django.contrib.staticfiles.finders import find
from django.utils.safestring import SafeString

register = template.Library()


@register.simple_tag
def embed_svg(static_path):
    return SafeString(Path(find(static_path)).read_text())
